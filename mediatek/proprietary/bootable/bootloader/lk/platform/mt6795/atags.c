#include <reg.h>
#include <debug.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <platform.h>
#include <platform/mt_typedefs.h>
#include <platform/boot_mode.h>
#include <platform/mt_reg_base.h>
#include <platform/sec_devinfo.h>
#include <platform/env.h>
#include <platform/sec_export.h>
extern int g_nr_bank;
extern BOOT_ARGUMENT *g_boot_arg;
extern BI_DRAM bi_dram[MAX_NR_BANK];

struct tag_header {
	u32 size;
	u32 tag;
};

#define tag_size(type)  ((sizeof(struct tag_header) + sizeof(struct type)) >> 2)

#define SIZE_1M             (1024 * 1024)
#define SIZE_2M             (2 * SIZE_1M)
#define SIZE_256M           (256 * SIZE_1M)
#define SIZE_512M           (512 * SIZE_1M)

/* The list must start with an ATAG_CORE node */
#define ATAG_CORE   0x54410001
struct tag_core {
	u32 flags;      /* bit 0 = read-only */
	u32 pagesize;
	u32 rootdev;
};

/* it is allowed to have multiple ATAG_MEM nodes */
#define ATAG_MEM  0x54410002
typedef struct {
	uint32_t size;
	uint32_t start_addr;
} mem_info;

#define ATAG_MEM64  0x54420002
typedef struct {
	uint64_t size;
	uint64_t start_addr;
} mem64_info;

#define ATAG_EXT_MEM64  0x54430002
typedef struct {
	uint64_t size;
	uint64_t start_addr;
} ext_mem64_info;

/* command line: \0 terminated string */
#define ATAG_CMDLINE    0x54410009
struct tag_cmdline {
	char    cmdline[1]; /* this is the minimum size */
};

/* describes where the compressed ramdisk image lives (physical address) */
#define ATAG_INITRD2    0x54420005
struct tag_initrd {
	u32 start;  /* physical start address */
	u32 size;   /* size of compressed ramdisk image in bytes */
};

#define ATAG_VIDEOLFB   0x54410008
struct tag_videolfb {
	u64 fb_base;
	u32 islcmfound;
	u32 fps;
	u32 vram;
	char lcmname[1]; /* this is the minimum size */
};

/* boot information */
#define ATAG_BOOT   0x41000802
struct tag_boot {
	u32 bootmode;
};

/*META com port information*/
#define ATAG_META_COM 0x41000803
struct tag_meta_com {
	u32 meta_com_type; /* identify meta via uart or usb */
	u32 meta_com_id;  /* multiple meta need to know com port id */
};

/*device information*/
#define ATAG_DEVINFO_DATA         0x41000804
#define ATAG_DEVINFO_DATA_SIZE    38
struct tag_devinfo_data {
	u32 devinfo_data[ATAG_DEVINFO_DATA_SIZE];
	u32 devinfo_data_size;
};

#define ATAG_MDINFO_DATA 0x41000806
struct tag_mdinfo_data {
	u8 md_type[4];
};

#define ATAG_VCORE_DVFS_INFO 0x54410007
struct tag_vcore_dvfs_info {
	u32 pllgrpreg_size;
	u32 freqreg_size;
	u32* low_freq_pll_val;
	u32* low_freq_cha_val;
	u32* low_freq_chb_val;
	u32* high_freq_pll_val;
	u32* high_freq_cha_val;
	u32* high_freq_chb_val;
};
#if (6795 == MACH_TYPE)
#define ATAG_PTP_INFO 0x54410008
struct tag_ptp_info {
	u8 first_volt;
	u8 second_volt;
};
#endif
/* The list ends with an ATAG_NONE node. */
#define ATAG_NONE   0x00000000

unsigned *target_atag_nand_data(unsigned *ptr)
{
	return ptr;
}


unsigned *target_atag_partition_data(unsigned *ptr)
{
	return ptr;
}

unsigned *target_atag_boot(unsigned *ptr)
{
	*ptr++ = tag_size(tag_boot);
	*ptr++ = ATAG_BOOT;
	*ptr++ = g_boot_mode;

	return ptr;
}

unsigned *target_atag_devinfo_data(unsigned *ptr)
{
	int i = 0;
	*ptr++ = tag_size(tag_devinfo_data);
	*ptr++ = ATAG_DEVINFO_DATA;
	for (i=0; i<ATAG_DEVINFO_DATA_SIZE; i++) {
		*ptr++ = get_devinfo_with_index(i);
	}
	*ptr++ = ATAG_DEVINFO_DATA_SIZE;
	dprintf(INFO, "SSSS:0x%x\n", get_devinfo_with_index(1));
	dprintf(INFO, "SSSS:0x%x\n", get_devinfo_with_index(2));
	dprintf(INFO, "SSSS:0x%x\n", get_devinfo_with_index(3));
	dprintf(INFO, "SSSS:0x%x\n", get_devinfo_with_index(4));
	dprintf(INFO, "SSSS:0x%x\n", get_devinfo_with_index(20));
	dprintf(INFO, "SSSS:0x%x\n", get_devinfo_with_index(21));

	return ptr;
}

unsigned *target_atag_masp_data(unsigned *ptr)
{
	/*tag size*/
	*ptr++ = tag_size(tag_masp_data);
	/*tag name*/
	*ptr++ = ATAG_MASP_DATA;
	ptr = fill_atag_masp_data(ptr);

	return ptr;
}

unsigned *target_atag_mem(unsigned *ptr)
{
	int i;

	for (i = 0; i < g_nr_bank; i++) {
#ifndef MTK_LM_MODE
		*ptr++ = 4; //tag size
		*ptr++ = ATAG_MEM; //tag name
		*ptr++ = bi_dram[i].size;
		*ptr++ = bi_dram[i].start;
#else
		*ptr++ = 6; //tag size
		*ptr++ = ATAG_MEM64; //tag name
		//*((unsigned long long*)ptr)++ = bi_dram[i].size;
		//*((unsigned long long*)ptr)++ = bi_dram[i].start;
		unsigned long long *ptr64 = (unsigned long long *)ptr;
		*ptr64++ = bi_dram[i].size;
		*ptr64++ = bi_dram[i].start;
		ptr = (unsigned int *)ptr64;
#endif
	}
	return ptr;
}

unsigned *target_atag_meta(unsigned *ptr)
{
	*ptr++ = tag_size(tag_meta_com);
	*ptr++ = ATAG_META_COM;
	*ptr++ = g_boot_arg->meta_com_type;
	*ptr++ = g_boot_arg->meta_com_id;
	dprintf(CRITICAL, "meta com type = %d\n", g_boot_arg->meta_com_type);
	dprintf(CRITICAL, "meta com id = %d\n", g_boot_arg->meta_com_id);
	return ptr;
}

/* todo: give lk strtoul and nuke this */
static unsigned hex2unsigned(const char *x)
{
	unsigned n = 0;

	while (*x) {
		switch (*x) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				n = (n << 4) | (*x - '0');
				break;
			case 'a':
			case 'b':
			case 'c':
			case 'd':
			case 'e':
			case 'f':
				n = (n << 4) | (*x - 'a' + 10);
				break;
			case 'A':
			case 'B':
			case 'C':
			case 'D':
			case 'E':
			case 'F':
				n = (n << 4) | (*x - 'A' + 10);
				break;
			default:
				return n;
		}
		x++;
	}

	return n;
}

unsigned *target_atag_commmandline(unsigned *ptr, char *commandline)
{
	char *p;

	if (!commandline)
		return NULL;

	for (p = commandline; *p == ' '; p++);

	if (*p == '\0')
		return NULL;

	*ptr++ = (sizeof (struct tag_header) + strlen (p) + 1 + 4) >> 2;; //size
	*ptr++ = ATAG_CMDLINE;
	strcpy((char *)ptr, p);  //copy to atags memory region
	ptr += (strlen (p) + 1 + 4) >> 2;
	return ptr;
}

unsigned *target_atag_initrd(unsigned *ptr, ulong initrd_start, ulong initrd_size)
{
	*ptr++ = tag_size(tag_initrd);
	*ptr++ = ATAG_INITRD2;
//TMP for bring up testing
//  *ptr++ = CFG_RAMDISK_LOAD_ADDR;
// *ptr++ = 0x1072F9;

	*ptr++ = initrd_start;
	*ptr++ = initrd_size;
	return ptr;
}

#include <platform/mt_disp_drv.h>
#include <platform/disp_drv.h>
unsigned *target_atag_videolfb(unsigned *ptr)
{
	extern unsigned long long fb_addr_pa_k;
	const char   *lcmname = mt_disp_get_lcm_id();
	unsigned int *p       = NULL;
	unsigned long long *phy_p   = (unsigned long long *)ptr;
	*phy_p = fb_addr_pa_k;
	p = (unsigned int*)(phy_p + 1);
	*p++ = DISP_IsLcmFound();
	*p++ = mt_disp_get_lcd_time();
	*p++ = DISP_GetVRamSize();
	strcpy((char *)p,lcmname);
	p += (strlen(lcmname) + 1 + 4) >> 2;

	dprintf(CRITICAL, "videolfb - fb_base    = 0x%llx\n",fb_addr_pa_k);
	dprintf(CRITICAL, "videolfb - islcmfound = %d\n",DISP_IsLcmFound());
	dprintf(CRITICAL, "videolfb - fps        = %d\n",mt_disp_get_lcd_time());
	dprintf(CRITICAL, "videolfb - vram       = %d\n",DISP_GetVRamSize());
	dprintf(CRITICAL, "videolfb - lcmname    = %s\n",lcmname);

	return (unsigned *)p;

}


unsigned *target_atag_mdinfo(unsigned *ptr)
{
	unsigned char *p;
	*ptr++=tag_size(tag_mdinfo_data);
	*ptr++=ATAG_MDINFO_DATA;
	p=(unsigned char *)ptr;
	*p++=g_boot_arg->md_type[0];
	*p++=g_boot_arg->md_type[1];
	*p++=g_boot_arg->md_type[2];
	*p++=g_boot_arg->md_type[3];
	return (unsigned *)p;
}

unsigned *target_atag_vcore_dvfs(unsigned *ptr)
{
	vcore_dvfs_info_t* dvfs_info = &g_boot_arg->vcore_dvfs_info;
	u32 pll_setting_num = dvfs_info->pll_setting_num;
	u32 freq_setting_num = dvfs_info->freq_setting_num;

	*ptr++ = (sizeof(struct tag_header)+ (sizeof(u32) * 2) + (pll_setting_num * sizeof(u32) * 2) + (freq_setting_num * sizeof(u32) * 4)) >> 2;
	*ptr++ = ATAG_VCORE_DVFS_INFO;

	*ptr++ = pll_setting_num;
	*ptr++ = freq_setting_num;

	dprintf(CRITICAL, "[vcore dvfs][atag] p = 0x%x, src = 0x%x, low_freq_pll_val[0] = 0x%x\n", (unsigned int)ptr, (unsigned int)dvfs_info->low_freq_pll_setting_addr, *(u32*)(dvfs_info->low_freq_pll_setting_addr));
	memcpy((void *)ptr, (void *)dvfs_info->low_freq_pll_setting_addr, pll_setting_num * 4);
	ptr += pll_setting_num;

	dprintf(CRITICAL, "[vcore dvfs][atag] p = 0x%x, src = 0x%x, low_freq_cha_val[0] = 0x%x\n", (unsigned int)ptr, (unsigned int)dvfs_info->low_freq_cha_setting_addr, *(u32*)(dvfs_info->low_freq_cha_setting_addr));
	memcpy((void *)ptr, (void *)dvfs_info->low_freq_cha_setting_addr, freq_setting_num * 4);
	ptr += freq_setting_num;

	dprintf(CRITICAL, "[vcore dvfs][atag] p = 0x%x, src = 0x%x, low_freq_chb_val[0] = 0x%x\n", (unsigned int)ptr, (unsigned int)dvfs_info->low_freq_chb_setting_addr, *(u32*)(dvfs_info->low_freq_chb_setting_addr));
	memcpy((void *)ptr, (void *)dvfs_info->low_freq_chb_setting_addr, freq_setting_num * 4);
	ptr += freq_setting_num;

	dprintf(CRITICAL, "[vcore dvfs][atag] p = 0x%x, src = 0x%x, high_freq_pll_val[0] = 0x%x\n", (unsigned int)ptr, (unsigned int)dvfs_info->high_freq_pll_setting_addr, *(u32*)(dvfs_info->high_freq_pll_setting_addr));
	memcpy((void *)ptr, (void *)dvfs_info->high_freq_pll_setting_addr, pll_setting_num * 4);
	ptr += pll_setting_num;

	dprintf(CRITICAL, "[vcore dvfs][atag] p = 0x%x, src = 0x%x, high_freq_cha_val[0] = 0x%x\n", (unsigned int)ptr, (unsigned int)dvfs_info->high_freq_cha_setting_addr, *(u32*)(dvfs_info->high_freq_cha_setting_addr));
	memcpy((void *)ptr, (void *)dvfs_info->high_freq_cha_setting_addr, freq_setting_num * 4);
	ptr += freq_setting_num;

	dprintf(CRITICAL, "[vcore dvfs][atag] p = 0x%x, src = 0x%x, high_freq_chb_val[0] = 0x%x\n", (unsigned int)ptr, (unsigned int)dvfs_info->high_freq_chb_setting_addr, *(u32*)(dvfs_info->high_freq_chb_setting_addr));
	memcpy((void *)ptr, (void *)dvfs_info->high_freq_chb_setting_addr, freq_setting_num * 4);
	ptr += freq_setting_num;

	return ptr;
}

#if (6795 == MACH_TYPE)
unsigned *target_atag_ptp(unsigned *ptr)
{
	ptp_info_t* ptp_info = &g_boot_arg->ptp_volt_info;
	u8 first_volt = ptp_info->first_volt;
	u8 second_volt = ptp_info->second_volt;

	*ptr++=tag_size(tag_ptp_info);
	*ptr++=ATAG_PTP_INFO;

	*ptr++=first_volt;
	*ptr++=second_volt;

	return ptr;
}
#endif
void *target_get_scratch_address(void)
{
	return ((void *)SCRATCH_ADDR);
}

#ifdef DEVICE_TREE_SUPPORT
#include <libfdt.h>

#if (6795 == MACH_TYPE)
int target_fdt_model(void *fdt)
{
	unsigned int devinfo = get_devinfo_with_index(24);
	int code = (int)((devinfo >> 24) & 0xF);
	const struct fdt_property *prop;
	char *prop_name = "model";
	unsigned int len,nodeoffset;
#if (6795 == MACH_TYPE)
	ptp_info_t* ptp_info = &g_boot_arg->ptp_volt_info;
	u8 first_volt = ptp_info->first_volt;
	u8 second_volt = ptp_info->second_volt;
	dprintf(CRITICAL, "[PTP][LK] first_volt = 0x%X\n", first_volt);
	dprintf(CRITICAL, "[PTP][LK] second_volt = 0x%X\n", second_volt);
#endif
	/* Becuase the model is at the begin of device tree.
	 * use nodeoffset=0
	 */
	nodeoffset = 0;
	prop = fdt_get_property(fdt, nodeoffset, prop_name, &len);
	if (prop) {
		int namestroff;
		const char *str;;
		switch (code) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				str = "MT6795M";
				break;
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
				str = "MT6795";
				break;
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				str = "MT6795T";
				break;
			case 0:
			default:
				str = "MT6795";
				break;
		}
		//printf("prop->data=0x%8x\n", (uint32_t *)prop->data);
		fdt_setprop_string(fdt, nodeoffset, prop_name, str);
		prop = fdt_get_property(fdt, nodeoffset, prop_name, &len);
		namestroff = fdt32_to_cpu(prop->nameoff);
		dprintf(CRITICAL, "%s=%s(%d)\n", fdt_string(fdt, namestroff), (char *)prop->data,code);
	}
	return 0;
}
#endif

#define NUM_CLUSTERS  2
#define NUM_CORES_CLUSTER0  4
#define NUM_CORES_CLUSTER1  4
#define COMPATIBLE_CLUSTER0  "arm,cortex-a7"
#define COMPATIBLE_CLUSTER1  "arm,cortex-a17"
#define MAX_CLK_FREQ_CLUSTER0  1700000000
#define MAX_CLK_FREQ_CLUSTER1  2200000000

int target_fdt_cpus(void *fdt)
{
	int cpus_offset, cpu_node, last_node = -1;
	int len;
	const struct fdt_property *prop;
	unsigned int reg, clk_freq;

	unsigned int cluster_idx;
	unsigned int core_num, cluster_num, lowest_cluster = NUM_CLUSTERS;

	unsigned int activated_cores[NUM_CLUSTERS] = {0};
	unsigned int available_cores[NUM_CLUSTERS] = {NUM_CORES_CLUSTER0, NUM_CORES_CLUSTER1};
	char *compatible[NUM_CLUSTERS] = {COMPATIBLE_CLUSTER0, COMPATIBLE_CLUSTER1};
	unsigned int max_clk_freq[NUM_CLUSTERS] = {MAX_CLK_FREQ_CLUSTER0, MAX_CLK_FREQ_CLUSTER1};

	if (mt_get_chip_hw_code() == 0x6795) {
		/*TO-DO do nothing about CPU number change in MT6795*/
	} else {
		if (mt_get_chip_sw_ver() >= CHIP_SW_VER_02) {
			unsigned int core_bond = get_devinfo_with_index(3);
			switch ((core_bond & (0xF << 24)) >> 24) {
				case 0b0000:
					break;
				case 0b1000:
					available_cores[0] = 3;
					break;
				case 0b1100:
					available_cores[0] = 2;
					break;
				case 0b1110:
					available_cores[0] = 1;
					break;
				default:
					break;
			}

			switch ((core_bond & (0xF << 12)) >> 12) {
				case 0b0000:
					break;
				case 0b1000:
					available_cores[1] = 3;
					break;
				case 0b1100:
					available_cores[1] = 2;
					break;
				case 0b1110:
					available_cores[1] = 1;
					break;
				default:
					break;
			}

			switch (core_bond & 0x7) {
				case 0b000:
					break;
				case 0b101:
					max_clk_freq[0] = 1500000000;
					max_clk_freq[1] = 2000000000;
					break;
				default:
					break;
			}
		} else {
#ifndef MTK_FORCE_CLUSTER1
			available_cores[1] = 0;
#else
			available_cores[0] = 0;
#endif
			max_clk_freq[0] = 1690000000;
			max_clk_freq[1] = 1898000000;
		}

		cpus_offset = fdt_path_offset(fdt, "/cpus");
		if (cpus_offset < 0) {
			printf("couldn't find /cpus\n");
			return cpus_offset;
		}

		for (cpu_node = fdt_first_subnode(fdt, cpus_offset); cpu_node >= 0;
		        cpu_node = ((last_node >= 0) ? fdt_next_subnode(fdt, last_node) : fdt_first_subnode(fdt, cpus_offset))) {
			prop = fdt_get_property(fdt, cpu_node, "device_type", &len);
			if ((!prop) || (len < 4) || (strcmp(prop->data, "cpu"))) {
				last_node = cpu_node;
				continue;
			}

			for (cluster_idx = 0; cluster_idx < NUM_CLUSTERS; cluster_idx++) {
				if (fdt_node_check_compatible(fdt, cpu_node, compatible[cluster_idx]) == 0) {
					prop = fdt_get_property(fdt, cpu_node, "reg", &len);
					reg = fdt32_to_cpu(*(unsigned int *)prop->data);
					core_num = reg & 0xFF;
					if (core_num >= available_cores[cluster_idx]) {
						printf("delete: cluster = %d, core = %d\n", cluster_idx, core_num);
						fdt_del_node(fdt, cpu_node);
					}
					/* ========== */
					else {
						activated_cores[cluster_idx]++;

						prop = fdt_get_property(fdt, cpu_node, "clock-frequency", &len);
						clk_freq = fdt32_to_cpu(*(unsigned int *)prop->data);
						if (clk_freq > max_clk_freq[cluster_idx]) {
							printf("setprop: clock-frequency = %u => %u\n", clk_freq, max_clk_freq[cluster_idx]);
							fdt_setprop_cell(fdt, cpu_node, "clock-frequency", max_clk_freq[cluster_idx]);
						}

						cluster_num = (reg & (0xFF << 8)) >> 8;
						lowest_cluster = (cluster_num < lowest_cluster) ? cluster_num : lowest_cluster;

						last_node = cpu_node;
					}
					/* ========== */
					break;
				}
			}

			if (cluster_idx == NUM_CLUSTERS) {
				printf("Warning: unknown cpu type in device tree\n");
				last_node = cpu_node;
			}
		}

		for (cluster_idx = 0; cluster_idx < NUM_CLUSTERS; cluster_idx++) {
			if (activated_cores[cluster_idx] > available_cores[cluster_idx])
				printf("Warning: unexpected reg value in device tree\n");
		}

		/* ========== */
		if (lowest_cluster != 0) {
			for (cpu_node = fdt_first_subnode(fdt, cpus_offset); cpu_node >= 0; cpu_node = fdt_next_subnode(fdt, cpu_node)) {
				prop = fdt_get_property(fdt, cpu_node, "device_type", &len);
				if ((!prop) || (len < 4) || (strcmp(prop->data, "cpu")))
					continue;

				prop = fdt_get_property(fdt, cpu_node, "reg", &len);
				reg = fdt32_to_cpu(*(unsigned int *)prop->data);
				printf("setprop: reg = 0x%08X => ", reg);
				cluster_num = (reg & (0xFF << 8)) >> 8;
				reg = (reg & ~(0xFF << 8)) | ((cluster_num - lowest_cluster) << 8);
				printf("0x%08X\n", reg);
				fdt_setprop_cell(fdt, cpu_node, "reg", reg);
			}
		}
		/* ========== */
	}
	return 0;
}
#endif


int target_fdt_firmware(void *fdt, char *serialno)
{
	int nodeoffset, namestroff, len;
	char *name, *value;
	const struct fdt_property *prop;

	nodeoffset = fdt_add_subnode(fdt, 0, "firmware");

	if (nodeoffset < 0) {
		dprintf(CRITICAL, "Warning: can't add firmware node in device tree\n");
		return -1;
	}

	nodeoffset = fdt_add_subnode(fdt, nodeoffset, "android");

	if (nodeoffset < 0) {
		dprintf(CRITICAL, "Warning: can't add firmware/android node in device tree\n");
		return -1;
	}

	name = "compatible";
	value = "android,firmware";
	fdt_setprop_string(fdt, nodeoffset, name, value);

	name = "hardware";
	value = PLATFORM;
	{
		char *tmp_str = value;
		for ( ; *tmp_str; tmp_str++ ) {
			*tmp_str = tolower(*tmp_str);
		}
	}
	fdt_setprop_string(fdt, nodeoffset, name, value);

	name = "serialno";
	value = serialno;
	fdt_setprop_string(fdt, nodeoffset, name, value);
	name = "mode";

	switch (g_boot_mode) {
		case META_BOOT:
		case ADVMETA_BOOT:
			value = "meta";
			break;
#if defined (MTK_KERNEL_POWER_OFF_CHARGING)
		case KERNEL_POWER_OFF_CHARGING_BOOT:
		case LOW_POWER_OFF_CHARGING_BOOT:
			value = "charger";
			break;
#endif
		case FACTORY_BOOT:
		case ATE_FACTORY_BOOT:
			value = "factory";
			break;
		case RECOVERY_BOOT:
			value = "recovery";
			break;
		default:
			value = "normal";
			break;
	}

	fdt_setprop_string(fdt, nodeoffset, name, value);

	return 0;
}

#ifdef NEW_MEMORY_RESERVED_MODEL
int get_mblock_num(void)
{
	return g_boot_arg->mblock_info.mblock_num;
}

int setup_mem_property_use_mblock_info(dt_dram_info *property, size_t p_size)
{
	mblock_info_t *mblock_info = &g_boot_arg->mblock_info;
	dt_dram_info *p;
	int i;

	if (mblock_info->mblock_num > p_size) {
		dprintf(CRITICAL, "mblock_info->mblock_num =%d is bigger than mem_property=%d\n", mblock_info->mblock_num, p_size);
		return 1;
	}

	for (i = 0; i < mblock_info->mblock_num; ++i) {
		p = (property + i);

		dprintf(CRITICAL, "p=0x%08x\n", p);

		p->start_hi = cpu_to_fdt32(mblock_info->mblock[i].start>>32);
		p->start_lo = cpu_to_fdt32(mblock_info->mblock[i].start);
		p->size_hi = cpu_to_fdt32((mblock_info->mblock[i].size)>>32);
		p->size_lo = cpu_to_fdt32(mblock_info->mblock[i].size);
		dprintf(CRITICAL, "mblock[%d].start: 0x%llx, size: 0x%llx\n",
		        i,
		        mblock_info->mblock[i].start,
		        mblock_info->mblock[i].size);

		dprintf(CRITICAL, " mem_reg_property[%d].start_hi = 0x%08X\n", i, p->start_hi);
		dprintf(CRITICAL, " mem_reg_property[%d].start_lo = 0x%08X\n", i, p->start_lo);
		dprintf(CRITICAL, " mem_reg_property[%d].size_hi  = 0x%08X\n", i, p->size_hi);
		dprintf(CRITICAL, " mem_reg_property[%d].size_lo  = 0x%08X\n", i, p->size_lo);
	}

	return 0;
}


int platform_atag_append(void *fdt)
{
	char *ptr;
	int offset;
	int ret = 0;

	offset = fdt_path_offset(fdt, "/memory");

	ptr = (char *)&g_boot_arg->orig_dram_info;
	ret = fdt_setprop(fdt, offset, "orig_dram_info", ptr, sizeof(dram_info_t));
	if (ret) goto exit;

	ptr = (char *)&g_boot_arg->mblock_info;
	ret = fdt_setprop(fdt, offset, "mblock_info", ptr, sizeof(mblock_info_t));
	if (ret) goto exit;

	ptr = (char *)&g_boot_arg->lca_reserved_mem;
	ret = fdt_setprop(fdt, offset, "lca_reserved_mem", ptr, sizeof(mem_desc_t));
	if (ret) goto exit;

	ptr = (char *)&g_boot_arg->tee_reserved_mem;
	ret = fdt_setprop(fdt, offset, "tee_reserved_mem", ptr, sizeof(mem_desc_t));
	if (ret) goto exit;

exit:

	if (ret)
		return 1;

	return 0;
}
#endif // NEW_MEMORY_RESERVED_MODEL
