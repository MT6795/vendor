#include <platform/mt_typedefs.h>
#include <platform/mt_reg_base.h>
#include <platform/mt_pmic.h>
#include <platform/mt_gpt.h>
#include <platform/mt_pmic_wrap_init.h>
#include <printf.h>
#include <platform/upmu_hw.h>
#include <platform/upmu_common.h>

//==============================================================================
// Global variable
//==============================================================================
int Enable_PMIC_LOG = 1;

CHARGER_TYPE g_ret = CHARGER_UNKNOWN;
int g_charger_in_flag = 0;
int g_first_check=0;

extern int g_R_BAT_SENSE;    
extern int g_R_I_SENSE;
extern int g_R_CHARGER_1;
extern int g_R_CHARGER_2;

//==============================================================================
// PMIC-AUXADC related define
//==============================================================================
#define VOLTAGE_FULL_RANGE     	3200
#define ADC_PRECISE         	4096 	// 12 bits
#define ADC_PRECISE_CH7     	32768 	// 15 bits
//==============================================================================
// PMIC-AUXADC global variable
//==============================================================================
kal_int32 count_time_out=1000;

//==============================================================================
// PMIC access API
//==============================================================================
U32 pmic_read_interface (U32 RegNum, U32 *val, U32 MASK, U32 SHIFT)
{
    U32 return_value = 0;    
    U32 pmic_reg = 0;
    U32 rdata;    

    //mt_read_byte(RegNum, &pmic_reg);
    return_value= pwrap_wacs2(0, (RegNum), 0, &rdata);
    pmic_reg=rdata;
    if(return_value!=0)
    {   
        dprintf(INFO, "[pmic_read_interface] Reg[%x]= pmic_wrap read data fail\n", RegNum);
        return return_value;
    }
    //dprintf(INFO, "[pmic_read_interface] Reg[%x]=0x%x\n", RegNum, pmic_reg);
    
    pmic_reg &= (MASK << SHIFT);
    *val = (pmic_reg >> SHIFT);    
    //dprintf(INFO, "[pmic_read_interface] val=0x%x\n", *val);

    return return_value;
}

U32 pmic_config_interface (U32 RegNum, U32 val, U32 MASK, U32 SHIFT)
{
    U32 return_value = 0;    
    U32 pmic_reg = 0;
    U32 rdata;

    //1. mt_read_byte(RegNum, &pmic_reg);
    return_value= pwrap_wacs2(0, (RegNum), 0, &rdata);
    pmic_reg=rdata;    
    if(return_value!=0)
    {   
        dprintf(INFO, "[pmic_config_interface] Reg[%x]= pmic_wrap read data fail\n", RegNum);
        return return_value;
    }
    //dprintf(INFO, "[pmic_config_interface] Reg[%x]=0x%x\n", RegNum, pmic_reg);
    
    pmic_reg &= ~(MASK << SHIFT);
    pmic_reg |= (val << SHIFT);

    //2. mt_write_byte(RegNum, pmic_reg);
    return_value= pwrap_wacs2(1, (RegNum), pmic_reg, &rdata);
    if(return_value!=0)
    {   
        dprintf(INFO, "[pmic_config_interface] Reg[%x]= pmic_wrap read data fail\n", RegNum);
        return return_value;
    }
    //dprintf(INFO, "[pmic_config_interface] write Reg[%x]=0x%x\n", RegNum, pmic_reg);    

#if 0
    //3. Double Check    
    //mt_read_byte(RegNum, &pmic_reg);
    return_value= pwrap_wacs2(0, (RegNum), 0, &rdata);
    pmic_reg=rdata;    
    if(return_value!=0)
    {   
        dprintf(INFO, "[pmic_config_interface] Reg[%x]= pmic_wrap write data fail\n", RegNum);
        return return_value;
    }
    dprintf(INFO, "[pmic_config_interface] Reg[%x]=0x%x\n", RegNum, pmic_reg);
#endif    

    return return_value;
}

//==============================================================================
// PMIC Usage APIs
//==============================================================================
U32 get_mt6331_pmic_chip_version (void)
{
    U32 val=0;

    pmic_read_interface( (kal_uint32)(MT6331_SWCID),
                           (&val),
                           (kal_uint32)(MT6331_PMIC_SWCID_MASK),
                           (kal_uint32)(MT6331_PMIC_SWCID_SHIFT)
                           );                           

    return val;
}

U32 get_mt6332_pmic_chip_version (void)
{
    U32 val=0;

    pmic_read_interface( (kal_uint32)(MT6332_SWCID),
                           (&val),
                           (kal_uint32)(MT6332_PMIC_SWCID_MASK),
                           (kal_uint32)(MT6332_PMIC_SWCID_SHIFT)
                           );                           

    return val;
}

kal_bool upmu_is_chr_det(void)
{
    U32 tmp32=0;
    
    #if 0
    tmp32 = 1; // for bring up
    #else
    pmic_config_interface(0x10A, 0x1,  0xF,  8);
    pmic_config_interface(0x10A, 0x17, 0xFF, 0);
    pmic_read_interface(0x108, &tmp32, 0x1,  1);
    #endif
    
    dprintf(CRITICAL, "[upmu_is_chr_det] %d\n", tmp32);
    
    if(tmp32 == 0)
    {        
        return KAL_FALSE;
    }
    else
    {    
        return KAL_TRUE;
    }
}

kal_bool pmic_chrdet_status(void)
{
    if( upmu_is_chr_det() == KAL_TRUE )    
    {
        #ifndef USER_BUILD
        dprintf(INFO, "[pmic_chrdet_status] Charger exist\r\n");
        #endif
        
        return KAL_TRUE;
    }
    else
    {
        #ifndef USER_BUILD
        dprintf(INFO, "[pmic_chrdet_status] No charger\r\n");
        #endif
        
        return KAL_FALSE;
    }
}

int pmic_detect_powerkey(void)
{
    U32 ret=0;
    U32 val=0;

    ret=pmic_read_interface( (U32)(MT6331_TOPSTATUS),
                             (&val),
                             (U32)(MT6331_PMIC_PWRKEY_DEB_MASK),
                             (U32)(MT6331_PMIC_PWRKEY_DEB_SHIFT)
                             );                            

    if(Enable_PMIC_LOG>1) 
        dprintf(INFO, "%d", ret);

    if (val==1){
        #ifndef USER_BUILD
        dprintf(INFO, "LK pmic powerkey Release\n");
        #endif
        
        return 0;
    }else{
        #ifndef USER_BUILD
        dprintf(INFO, "LK pmic powerkey Press\n");
        #endif
        
        return 1;
    }
}

int pmic_detect_homekey(void)
{
    U32 ret=0;
    U32 val=0;

    ret=pmic_read_interface( (U32)(MT6331_TOPSTATUS),
                             (&val),
                             (U32)(MT6331_PMIC_HOMEKEY_DEB_MASK),
                             (U32)(MT6331_PMIC_HOMEKEY_DEB_SHIFT)
                             );                           

    if(Enable_PMIC_LOG>1) 
        dprintf(INFO, "%d", ret);

    if (val==1){     
        #ifndef USER_BUILD
        dprintf(INFO, "LK pmic HOMEKEY Release\n");
        #endif
        
        return 0;
    }else{
        #ifndef USER_BUILD
        dprintf(INFO, "LK pmic HOMEKEY Press\n");
        #endif
        
        return 1;
    }
}

kal_uint32 upmu_get_reg_value(kal_uint32 reg)
{
    U32 ret=0;
    U32 temp_val=0;

    ret=pmic_read_interface(reg, &temp_val, 0xFFFF, 0x0);

    if(Enable_PMIC_LOG>1) 
        dprintf(INFO, "%d", ret);

    return temp_val;
}

//==============================================================================
// PMIC Init Code
//==============================================================================
void PMIC_INIT_SETTING_V1(void)
{
    //dprintf(INFO, "[LK_PMIC_INIT_SETTING_V1] Done\n");
}

void PMIC_CUSTOM_SETTING_V1(void)
{
    //dprintf(INFO, "[LK_PMIC_CUSTOM_SETTING_V1] Done\n");
}

U32 pmic_init (void)
{
    U32 ret_code = PMIC_TEST_PASS;

    dprintf(CRITICAL, "[pmic_init] LK Start..................\n");    
    dprintf(CRITICAL, "[pmic_init] MT6331 CHIP Code = 0x%x\n", get_mt6331_pmic_chip_version());
    dprintf(CRITICAL, "[pmic_init] MT6332 CHIP Code = 0x%x\n", get_mt6332_pmic_chip_version());

    PMIC_INIT_SETTING_V1();
    PMIC_CUSTOM_SETTING_V1();

    //pmic_chrdet_status();

    return ret_code;
}

//==============================================================================
// PMIC API for LK : AUXADC
//==============================================================================
static kal_uint32  pmic_is_auxadc_ready(kal_int32 channel_num, upmu_adc_chip_list_enum chip_num, upmu_adc_user_list_enum user_num)
{
#if 1	
	kal_uint32 int_status_val_0=0;
	
	if ( chip_num == MT6331_CHIP ) {
		if ( channel_num == 8 && user_num == GPS ) {
			pmic_read_interface(MT6331_AUXADC_ADC14,(&int_status_val_0),0x8000,0x0);
		} else if ( channel_num == 7 && user_num == MD ) {
			pmic_read_interface(MT6331_AUXADC_ADC15,(&int_status_val_0),0x8000,0x0);
		} else if ( channel_num == 7 && user_num == AP ) {
			pmic_read_interface(MT6331_AUXADC_ADC16,(&int_status_val_0),0x8000,0x0);
		} else if ( channel_num == 4 && user_num == MD ) {
			pmic_read_interface(MT6331_AUXADC_ADC17,(&int_status_val_0),0x8000,0x0);
		} else {
			pmic_read_interface(MT6331_AUXADC_ADC0 + (channel_num * 2),(&int_status_val_0),0x8000,0x0);
		}
	} else if( chip_num == MT6332_CHIP ) {
		if ( channel_num == 4 && user_num == MD ) {
			pmic_read_interface(MT6332_AUXADC_ADC17,(&int_status_val_0),0x8000,0x0);
		} else {
			pmic_read_interface(MT6332_AUXADC_ADC0 + (channel_num * 2),(&int_status_val_0),0x8000,0x0);
		}
	}
	
	return int_status_val_0 >> 15;
#else
    return 0;
#endif    
}

static kal_uint32  pmic_get_adc_output(kal_int32 channel_num, upmu_adc_chip_list_enum chip_num, upmu_adc_user_list_enum user_num)
{
#if 1	
	kal_uint32 int_status_val_0=0;
	
	if ( chip_num == MT6331_CHIP ) {
		if ( channel_num == 8 && user_num == GPS ) {
			pmic_read_interface(MT6331_AUXADC_ADC14,(&int_status_val_0),0x7FFF,0x0);
		} else if ( channel_num == 7 && user_num == MD ) {
			pmic_read_interface(MT6331_AUXADC_ADC15,(&int_status_val_0),0x7FFF,0x0);
		} else if ( channel_num == 7 && user_num == AP ) {
			pmic_read_interface(MT6331_AUXADC_ADC16,(&int_status_val_0),0x7FFF,0x0);
		} else if ( channel_num == 4 && user_num == MD ) {
			pmic_read_interface(MT6331_AUXADC_ADC17,(&int_status_val_0),0x7FFF,0x0);
		} else {
			pmic_read_interface(MT6331_AUXADC_ADC0 + (channel_num * 2),(&int_status_val_0),0x0FFF,0x0);
		}
	} else if ( chip_num == MT6332_CHIP ) {
		if ( channel_num == 4 && user_num == MD ) {
			pmic_read_interface(MT6332_AUXADC_ADC17,(&int_status_val_0),0x0FFF,0x0);
		} else if ( channel_num == 7  && user_num == AP ) {
			pmic_read_interface(MT6332_AUXADC_ADC16,(&int_status_val_0),0x7FFF,0x0);
		} else {
			pmic_read_interface(MT6332_AUXADC_ADC0 + (channel_num * 2),(&int_status_val_0),0x0FFF,0x0);
		}
	}
	return int_status_val_0;
#else
    return 0;
#endif    
}

static kal_uint32 PMIC_IMM_RequestAuxadcChannel(kal_int32 channel_num, upmu_adc_chip_list_enum chip_num, upmu_adc_user_list_enum user_num, int mode)
{
#if 1
	kal_uint32 ret = 0;
	
	if (user_num >= AUX_USER_MAX || chip_num >= ADC_CHIP_MAX)
		return 0;
		
	if ( chip_num == MT6331_CHIP ) {
		if (user_num == AP) {
			if (mode == 0) {
				ret=pmic_config_interface( (kal_uint32)(MT6331_AUXADC_RQST0_CLR), 0x1, 0x1, channel_num);
				ret=pmic_config_interface( (kal_uint32)(MT6331_AUXADC_RQST0_CLR), 0x0, 0x1, channel_num);
			} else if (mode == 1) {
		        		ret=pmic_config_interface( (kal_uint32)(MT6331_AUXADC_RQST0_SET), 0x1, 0x1, channel_num);
		        	} else if (mode == 2){
		        		ret=pmic_config_interface( (kal_uint32)(MT6331_AUXADC_RQST0), 0x1, 0x1, channel_num);	
		        	}
		} else if ( (user_num == MD && ( channel_num == 4 || channel_num == 7 )) || (user_num == GPS && channel_num == 8) ) {
			if (mode == 0) {
				ret=pmic_config_interface( (kal_uint32)(MT6331_AUXADC_RQST1_CLR), 0x1, 0x1, channel_num);
				ret=pmic_config_interface( (kal_uint32)(MT6331_AUXADC_RQST1_CLR), 0x0, 0x1, channel_num);
			} else if (mode == 1) {
				ret=pmic_config_interface( (kal_uint32)(MT6331_AUXADC_RQST1_SET), 0x1, 0x1, channel_num);
			} else if (mode == 2){
				ret=pmic_config_interface( (kal_uint32)(MT6331_AUXADC_RQST1), 0x1, 0x1, channel_num);
		        }
		} else {
			return 0;	
		}
	} else if ( chip_num == MT6332_CHIP ) {
		if (user_num == AP) {
			if (mode == 0) {
				ret=pmic_config_interface( (kal_uint32)(MT6332_AUXADC_RQST0_CLR), 0x1, 0x1, channel_num);
				ret=pmic_config_interface( (kal_uint32)(MT6332_AUXADC_RQST0_CLR), 0x0, 0x1, channel_num);
			} else if (mode == 1) {
				ret=pmic_config_interface( (kal_uint32)(MT6332_AUXADC_RQST0_SET), 0x1, 0x1, channel_num);
			} else if (mode == 2){
				ret=pmic_config_interface( (kal_uint32)(MT6332_AUXADC_RQST0), 0x1, 0x1, channel_num);		
			}
		} else if ( user_num == MD &&  channel_num == 4 ) {
			if (mode == 0) {
				ret=pmic_config_interface( (kal_uint32)(MT6332_AUXADC_RQST1_CLR), 0x1, 0x1, channel_num);
				ret=pmic_config_interface( (kal_uint32)(MT6332_AUXADC_RQST1_CLR), 0x0, 0x1, channel_num);
			} else if (mode == 1) {
				ret=pmic_config_interface( (kal_uint32)(MT6332_AUXADC_RQST1_SET), 0x1, 0x1, channel_num);
			} else if (mode == 2){
				ret=pmic_config_interface( (kal_uint32)(MT6332_AUXADC_RQST1), 0x1, 0x1, channel_num);	
			}
		} else {
			return 0;	
		}
	}
	return ret;
#else
	return 0;
#endif   
}
//==============================================================================
// PMIC-AUXADC 
//==============================================================================
int PMIC_IMM_GetOneChannelValue(upmu_adc_chl_list_enum dwChannel, int deCount, int trimd)
{
#if 1
	kal_int32 ret_data;    
	kal_int32 count=0;
	kal_int32 u4Sample_times = 0;
	kal_int32 u4channel=0;    
	kal_int32 adc_result_temp=0;
	kal_int32 r_val_temp=0;   
	kal_int32 adc_result=0;   
	kal_int32 channel_num;
	upmu_adc_chip_list_enum chip_num;
	upmu_adc_user_list_enum user_num;
	
	/*
		MT6331
		0 : NA
		1 : NA
		2 : NA 
		3 : NA
		4 : TSENSE_PMIC_31
		5 : VACCDET
		6 : VISMPS_1
		7 : AUXADCVIN0
		8 : NA    
		9 : HP
		11-15: Shared
		
		MT6332
		0 : BATSNS
		1 : ISENSE
		2 : VBIF 
		3 : BATON
		4 : TSENSE_PMIC_32
		5 : VCHRIN
		6 : VISMPS_2
		7 : VUSB/ VADAPTOR
		8 : M3_REF    
		9 : SPK_ISENSE
		10: SPK_THR_V
		11: SPK_THR_I
		12-15: shared 
	*/
	
	if(get_mt6332_pmic_chip_version()==PMIC6332_E1_CID_CODE) {
	// for batses, isense
	mt6332_upmu_set_rg_adcin_batsns_en(1);
	mt6332_upmu_set_rg_adcin_cs_en(1);
	}

#if 0
	//for baton
	pmic_config_interface(0x001E, 0x0, 0xffff, 0);
	pmic_config_interface(0x000E, 0xffff, 0xffff, 0);
	pmic_config_interface(0x8c20, 0x0, 0xffff, 0);
	pmic_config_interface(0x8c12, 0xffff, 0xffff, 0);
    	
	mt6332_upmu_set_rg_vbif28_on_ctrl(0);
	mt6332_upmu_set_rg_vbif28_en(1);
#endif			
	//for tses_32
	mt6331_upmu_set_rg_vbuf_en(1);
	mt6332_upmu_set_rg_vbuf_en(1);

	do
	{
		count=0;
	        ret_data=0;

		channel_num = (dwChannel & (AUXADC_CHANNEL_MASK << AUXADC_CHANNEL_SHIFT)) >> AUXADC_CHANNEL_SHIFT ;
		chip_num = (upmu_adc_chip_list_enum)((dwChannel & (AUXADC_CHIP_MASK << AUXADC_CHIP_SHIFT)) >> AUXADC_CHIP_SHIFT );
		user_num = (upmu_adc_user_list_enum)((dwChannel & (AUXADC_USER_MASK << AUXADC_USER_SHIFT)) >> AUXADC_USER_SHIFT );
	
		
		
		if (channel_num == 7 && chip_num == MT6332_CHIP) {	
			pmic_config_interface( (kal_uint32)(MT6332_CHR_CON14), 0x1, MT6332_PMIC_RG_AUXADC_USB_DET_MASK, MT6332_PMIC_RG_AUXADC_USB_DET_SHIFT);
			pmic_config_interface( (kal_uint32)(MT6332_CHR_CON14), 0x0, MT6332_PMIC_RG_AUXADC_DCIN_DET_MASK, MT6332_PMIC_RG_AUXADC_DCIN_DET_SHIFT);
		} else  if ( (channel_num == 7) && ((dwChannel & (0x20)) != 0) ){
			chip_num = 1;
			pmic_config_interface( (kal_uint32)(MT6332_CHR_CON14), 0x0, MT6332_PMIC_RG_AUXADC_USB_DET_MASK, MT6332_PMIC_RG_AUXADC_USB_DET_SHIFT);
			pmic_config_interface( (kal_uint32)(MT6332_CHR_CON14), 0x1, MT6332_PMIC_RG_AUXADC_DCIN_DET_MASK, MT6332_PMIC_RG_AUXADC_DCIN_DET_SHIFT);
		}
		
		if (user_num == GPS && chip_num == MT6331_CHIP) {
			mt6331_upmu_set_auxadc_ck_aon(1);
			mt6331_upmu_set_auxadc_ck_aon_md(0);
			mt6331_upmu_set_auxadc_ck_aon_gps(0);
			mt6331_upmu_set_auxadc_data_reuse_sel(0);
		} else {
			mt6331_upmu_set_auxadc_ck_aon(0);
			mt6331_upmu_set_auxadc_ck_aon_md(0);
			mt6331_upmu_set_auxadc_ck_aon_gps(0);
			mt6331_upmu_set_auxadc_data_reuse_sel(3);	
		}
#if 1		
		PMIC_IMM_RequestAuxadcChannel(channel_num, chip_num, user_num, CLEAR_REQ);		// clear
		PMIC_IMM_RequestAuxadcChannel(channel_num, chip_num, user_num, SET_REQ);		// set
#else
		PMIC_IMM_RequestAuxadcChannel(channel_num, chip_num, user_num, ONLY_REQ);		// request only
#endif
		udelay(1);
	        
	        
	
	        switch(dwChannel){         
	            case AUX_TSENSE_31_AP:	        
	            case AUX_VACCDET_AP:    
	            case AUX_VISMPS_1_AP:                 
	            case AUX_ADCVIN0_AP:    
	            case AUX_HP_AP:    
	            case AUX_BATSNS_AP:    
	            case AUX_ISENSE_AP:    
	            case AUX_VBIF_AP:    	                
	            case AUX_BATON_AP:                   
	            case AUX_TSENSE_32_AP:    
		    case AUX_VCHRIN_AP:  
		    case AUX_VISMPS_2_AP:
		    case AUX_VUSB_AP:  
		    case AUX_M3_REF_AP:
		    case AUX_SPK_ISENSE_AP:    
		    case AUX_SPK_THR_V_AP:  
		    case AUX_SPK_THR_I_AP: 
		    case AUX_VADAPTOR_AP: 
		    case AUX_TSENSE_31_MD:
		    case AUX_ADCVIN0_MD:
		    case AUX_ADCVIN0_GPS:
		    case AUX_TSENSE_32_MD:
	                while( pmic_is_auxadc_ready(channel_num, chip_num, user_num) != 1 )
	                {
		            mdelay(1);
		            if( (count++) > count_time_out)
		            {
		                        dprintf(INFO, "[IMM_GetOneChannelValue_PMIC] (%d) Time out!\n", dwChannel);
		                break;
		            }            
	                }
	                ret_data = pmic_get_adc_output(channel_num, chip_num, user_num);                
	                break; 
		    default:
		        dprintf(INFO, "[AUXADC] Invalid channel value(%d,%d)\n", dwChannel, trimd);
		        return -1;
		}
		
		PMIC_IMM_RequestAuxadcChannel(channel_num, chip_num, user_num, CLEAR_REQ);		// clear
		
	        u4channel += ret_data;
	
	        u4Sample_times++;
	
	            //debug
	        dprintf(INFO, "[AUXADC] u4channel[%d]=%d.\n", 
	                dwChannel, ret_data);
	        
	}while (u4Sample_times < deCount);

	/* Value averaging  */ 
	adc_result_temp = u4channel / deCount;
	
	switch(dwChannel){         
	case AUX_TSENSE_31_AP:                
	    r_val_temp = 1;           
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_VACCDET_AP:    
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_VISMPS_1_AP:    
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_ADCVIN0_AP:    
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE_CH7;
	    break;
	case AUX_HP_AP:    
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_BATSNS_AP:    
	    r_val_temp = 2;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_ISENSE_AP:    
	    r_val_temp = 2;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_VBIF_AP:    
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;    
	case AUX_BATON_AP:    
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;                
	case AUX_TSENSE_32_AP:  
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;    
	case AUX_VCHRIN_AP:  
	    r_val_temp = 10;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;  
	case AUX_VISMPS_2_AP:  
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_VUSB_AP: 
	    r_val_temp = 10;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE_CH7;
	    break;    
	case AUX_M3_REF_AP:
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;  
	case AUX_SPK_ISENSE_AP:    
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_SPK_THR_V_AP:  
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_SPK_THR_I_AP:  
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;
	case AUX_VADAPTOR_AP:
	    r_val_temp = 10;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE_CH7;
	    break;  
	case AUX_TSENSE_31_MD:  
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;  
	case AUX_ADCVIN0_MD:
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE_CH7;
	    break;  
	case AUX_ADCVIN0_GPS:
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE_CH7;
	    break;  
	case AUX_TSENSE_32_MD:        
	    r_val_temp = 1;
	    adc_result = (adc_result_temp*r_val_temp*VOLTAGE_FULL_RANGE)/ADC_PRECISE;
	    break;  
	default:
	    dprintf(INFO, "[AUXADC] Invalid channel value(%d,%d)\n", dwChannel, trimd);
	    return -1;
	}
	
	dprintf(INFO, "[AUXADC] adc_result_temp=%d, adc_result=%d, r_val_temp=%d.\n", 
	        adc_result_temp, adc_result, r_val_temp);
	
	
	return adc_result;
#else
	return 0;
#endif   
}


int get_bat_sense_volt(int times)
{
	return PMIC_IMM_GetOneChannelValue(AUX_BATSNS_AP,times,1);
}

int get_i_sense_volt(int times)
{
	return PMIC_IMM_GetOneChannelValue(AUX_ISENSE_AP,times,1);
}

int get_charger_volt(int times)
{
	return PMIC_IMM_GetOneChannelValue(AUX_VCHRIN_AP,times,1);
}

int get_tbat_volt(int times)
{
	return PMIC_IMM_GetOneChannelValue(AUX_BATON_AP,times,1);
}


