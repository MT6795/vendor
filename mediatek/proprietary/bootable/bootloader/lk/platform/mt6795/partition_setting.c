#include "platform/partition.h"


struct part_name_map g_part_name_map[PART_MAX_COUNT] = {
	{"preloader",	"preloader",	"raw data",	0,	0,	0},
	{"proinfo",	"proinfo",	"raw data",	1,	0,	0},
	{"nvram",	"nvram",	"raw data",	2,	0,	0},
	{"protect1",	"protect1",	"ext4",	        3,	0,	0},
	{"protect2",	"protect2",	"ext4",	        4,	0,	0},
	{"persist",	"persist",	"ext4",		5,	0,	0},
	{"seccfg",	"seccfg",	"raw data",	6,	0,	0},
	{"lk",		"lk",		"raw data",	7,	1,	1},
	{"boot",	"boot",		"raw data",	8,	1,	1},
	{"recovery",	"recovery",	"raw data",	9,	1,	1},
	{"secro",	"secro",	"raw data",	10,	0,	0},
	{"para",	"para",		"raw data",	11,	0,	0},
	{"logo",	"logo",		"raw data",	12,	0,	0},
	{"custom",	"custom",	"ext4",		13,	0,	0},
	{"frp",		"frp",		"raw data",	14,	0,	0},
	{"expdb",	"expdb",	"raw data",	15,	0,	0},
	{"system",	"system",	"ext4",		16,	1,	1},
	{"cache",	"cache",	"ext4",		17,	1,	1},
	{"userdata",	"userdata",	"ext4",		18,	1,	1},
	{"intsd",	"intsd",	"fat",		19,	0,	0},
	{"otp",		"otp",		"raw data",	20,	0,	0},
	{"flashinfo",	"flashinfo",	"raw data",	21,	0,	0},
        {""},
};
