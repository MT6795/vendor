#include <platform/mt_reg_base.h>
#include <platform/mt_typedefs.h>
#include <platform/mtk_wdt.h>
#include <platform/mt_gpt.h>
#include <platform/ddp_hal.h>
#include <platform/sec_devinfo.h>
#include <platform/boot_mode.h>
#include <printf.h>

#define REPAIR_SRAM_DEBUG  1
#if(REPAIR_SRAM_DEBUG == 2)
#define dbgmsg0 dprintf
#define dbgmsg1 dprintf
#elif(REPAIR_SRAM_DEBUG == 1)
#define dbgmsg0 dprintf
#define dbgmsg1(...)
#else
#define dbgmsg0(...)
#define dbgmsg1(...)
#endif


extern BOOT_ARGUMENT *g_boot_arg;


int repair_sram(void)
{
    // --------------------------------------------------
    // common
    // --------------------------------------------------
    int ret = 0;
    volatile UINT32 rdata;
    UINT32 rstd_reg;
    unsigned int bypass_md = 0;
    unsigned int modem_lmt = get_devinfo_with_index(4);
    unsigned int hspa_off = modem_lmt & (0x1 << 0);
    unsigned int tdd_off = modem_lmt & (0x1 << 7);
    //unsigned int lte_off = modem_lmt & (0x1 << 28);

    rdata = *((UINT32P)(0x10206178));
    dprintf(CRITICAL, "0x10206178 = 0x%x\n", rdata);

#if defined(MTK_KERNEL_POWER_OFF_CHARGING)
    if((g_boot_arg->maggic_number == BOOT_ARGUMENT_MAGIC) && ((g_boot_arg->boot_mode & 0x000000FF) == LOW_POWER_OFF_CHARGING_BOOT))
        bypass_md = 1;
#endif

    dprintf(CRITICAL, "repair_sram START\n");

#if 1
    //=== 0) set MTCMOS ==========================================================
    //efuse read enable
    *((UINT32P) (0x10206000)) = 0x00000004;
    // tddsys mbist_mem_clk_en (0x2400001e)
    *((UINT16P) (0x2400001e)) = 0x000d;
    //Turn On SPM Reg Key
    //*((UINT32P) (0x10006000)) = 0x0b160001;
    dbgmsg0(CRITICAL, "---- enable VDEC MTCMOS\n");
    *((UINT32P) (0x10006210)) = 0x00000f16;
    *((UINT32P) (0x10006210)) = 0x00000f1e;
    *((UINT32P) (0x10006210)) = 0x00000f0e;
    *((UINT32P) (0x10006210)) = 0x00000f0c;
    *((UINT32P) (0x10006210)) = 0x00000f0d;
    *((UINT32P) (0x10006210)) = 0x00000e0d;
    *((UINT32P) (0x10006210)) = 0x00000c0d;
    *((UINT32P) (0x10006210)) = 0x0000080d;
    *((UINT32P) (0x10006210)) = 0x0000000d;
    dbgmsg0(CRITICAL, "---- enable VENC MTCMOS\n");
    *((UINT32P) (0x10006230)) = 0x00000f16;
    *((UINT32P) (0x10006230)) = 0x00000f1e;
    *((UINT32P) (0x10006230)) = 0x00000f0e;
    *((UINT32P) (0x10006230)) = 0x00000f0c;
    *((UINT32P) (0x10006230)) = 0x00000f0d;
    *((UINT32P) (0x10006230)) = 0x00000e0d;
    *((UINT32P) (0x10006230)) = 0x00000c0d;
    *((UINT32P) (0x10006230)) = 0x0000080d;
    *((UINT32P) (0x10006230)) = 0x0000000d;
    dbgmsg0(CRITICAL, "---- enable MFG ASYNC MTCMOS\n");
    *((UINT32P) (0x100062c4)) = 0x00000f16;
    *((UINT32P) (0x100062c4)) = 0x00000f1e;
    *((UINT32P) (0x100062c4)) = 0x00000f0e;
    *((UINT32P) (0x100062c4)) = 0x00000f0c;
    *((UINT32P) (0x100062c4)) = 0x00000f0d;
    *((UINT32P) (0x100062c4)) = 0x00000e0d;
    *((UINT32P) (0x100062c4)) = 0x00000c0d;
    *((UINT32P) (0x100062c4)) = 0x0000080d;
    *((UINT32P) (0x100062c4)) = 0x0000000d;
    dbgmsg0(CRITICAL, "---- enable MFG 2D MTCMOS\n");
    *((UINT32P) (0x100062c0)) = 0x00000f16;
    *((UINT32P) (0x100062c0)) = 0x00000f1e;
    *((UINT32P) (0x100062c0)) = 0x00000f0e;
    *((UINT32P) (0x100062c0)) = 0x00000f0c;
    *((UINT32P) (0x100062c0)) = 0x00000f0d;
    *((UINT32P) (0x100062c0)) = 0x00000e0d;
    *((UINT32P) (0x100062c0)) = 0x00000c0d;
    *((UINT32P) (0x100062c0)) = 0x0000080d;
    *((UINT32P) (0x100062c0)) = 0x0000000d;
    dbgmsg0(CRITICAL, "---- enable MFG MTCMOS\n");
    *((UINT32P) (0x10006214)) = 0x00000f16;
    *((UINT32P) (0x10006214)) = 0x00000f1e;
    *((UINT32P) (0x10006214)) = 0x00000f0e;
    *((UINT32P) (0x10006214)) = 0x00000f0c;
    *((UINT32P) (0x10006214)) = 0x00000f0d;
    *((UINT32P) (0x10006214)) = 0x00000e0d;
    *((UINT32P) (0x10006214)) = 0x00000c0d;
    *((UINT32P) (0x10006214)) = 0x0000080d;
    *((UINT32P) (0x10006214)) = 0x0000000d;
    dbgmsg0(CRITICAL, "---- enable MJC MTCMOS\n");
    *((UINT32P) (0x10006298)) = 0x00000f16;
    *((UINT32P) (0x10006298)) = 0x00000f1e;
    *((UINT32P) (0x10006298)) = 0x00000f0e;
    *((UINT32P) (0x10006298)) = 0x00000f0c;
    *((UINT32P) (0x10006298)) = 0x00000f0d;
    *((UINT32P) (0x10006298)) = 0x00000e0d;
    *((UINT32P) (0x10006298)) = 0x00000c0d;
    *((UINT32P) (0x10006298)) = 0x0000080d;
    *((UINT32P) (0x10006298)) = 0x0000000d;
    dbgmsg0(CRITICAL, "---- enable ISP MTCMOS\n");
    *((UINT32P) (0x10006238)) = 0x00000f16;
    *((UINT32P) (0x10006238)) = 0x00000f1e;
    *((UINT32P) (0x10006238)) = 0x00000f0e;
    *((UINT32P) (0x10006238)) = 0x00000f0c;
    *((UINT32P) (0x10006238)) = 0x00000f0d;
    *((UINT32P) (0x10006238)) = 0x00000e0d;
    *((UINT32P) (0x10006238)) = 0x00000c0d;
    *((UINT32P) (0x10006238)) = 0x0000080d;
    *((UINT32P) (0x10006238)) = 0x0000000d;
    dbgmsg0(CRITICAL, "---- enable DISP MTCMOS\n");
    *((UINT32P) (0x1000623c)) = 0x00000f16;
    *((UINT32P) (0x1000623c)) = 0x00000f1e;
    *((UINT32P) (0x1000623c)) = 0x00000f0e;
    *((UINT32P) (0x1000623c)) = 0x00000f0c;
    *((UINT32P) (0x1000623c)) = 0x00000f0d;
    *((UINT32P) (0x1000623c)) = 0x00000e0d;
    *((UINT32P) (0x1000623c)) = 0x00000c0d;
    *((UINT32P) (0x1000623c)) = 0x0000080d;
    *((UINT32P) (0x1000623c)) = 0x0000000d;
    dbgmsg0(CRITICAL, "---- enable AUDIO MTCMOS\n");
    *((UINT32P) (0x1000629c)) = 0x00000f16;
    *((UINT32P) (0x1000629c)) = 0x00000f1e;
    *((UINT32P) (0x1000629c)) = 0x00000f0e;
    *((UINT32P) (0x1000629c)) = 0x00000f0c;
    *((UINT32P) (0x1000629c)) = 0x00000f0d;
    *((UINT32P) (0x1000629c)) = 0x00000e0d;
    *((UINT32P) (0x1000629c)) = 0x00000c0d;
    *((UINT32P) (0x1000629c)) = 0x0000080d;
    *((UINT32P) (0x1000629c)) = 0x0000000d;
    //RISCRead  : address 23010800 data 0000a200
    //RISCRead  : address 27010804 data 0000a200
    //RISCRead  : address 27010808 data 0000a200
    //RISCRead  : address 2701080c data 0000a200
    //RISCRead  : address 27010810 data 0000a200
if(!bypass_md)
{
    dbgmsg0(CRITICAL, "---- enable md2gsys MTCMOS\n");
    *((UINT32P) (0x23010800)) = 0x0000a244;
    dbgmsg0(CRITICAL, "---- enable hspasys1 MTCMOS\n");
    *((UINT32P) (0x27010804)) = 0x0000a244;
    dbgmsg0(CRITICAL, "---- enable hspasys2 MTCMOS\n");                                   //NEW!!!!!!!!TBD
    *((UINT32P) (0x27010808)) = 0x0000a244;
    dbgmsg0(CRITICAL, "---- enable hspasys3 MTCMOS\n");
    *((UINT32P) (0x2701080c)) = 0x0000a244;
    dbgmsg0(CRITICAL, "---- enable hspasys4 MTCMOS\n");
    *((UINT32P) (0x27010810)) = 0x0000a244;
    // MD_TOPSM_RM_TMR_PWR0
    *((UINT32P) (0x20030018)) = 0x00000000;
    // MODEM2G_TOPSM_RM_TMR_PWR0
    *((UINT32P) (0x23010018)) = 0x00000000;
    // MODEM3G_TOPSM_RM_TMR_PWR0
    *((UINT32P) (0x27010018)) = 0x00000000;
    dbgmsg0(CRITICAL, "---- setup PLL for MDSYS\n");
    *((UINT32P) (0x201200a0)) = 0x00000001;
    *((UINT32P) (0x201200ac)) = 0x000001ff;
    *((UINT32P) (0x2012004c)) = 0x00008300;
    *((UINT32P) (0x20120028)) = 0x00000070;
    *((UINT32P) (0x2012004c)) = 0x00000000;
    *((UINT32P) (0x20120028)) = 0x00000000;
    *((UINT32P) (0x20120048)) = 0x00000000;
    *((UINT32P) (0x20120024)) = 0x00000000;
    *((UINT32P) (0x20120700)) = 0x00000010;
    *((UINT32P) (0x20120050)) = 0x00000013;
    *((UINT32P) (0x20120054)) = 0x00000a25;
    //RISCRead  : address 20120100 data 0000470f
    //RISCRead  : address 20120140 data 00000410
    //RISCRead  : address 201201c0 data 00000800
    //RISCRead  : address 20120200 data 00000800
    //RISCRead  : address 20120150 data 00000810
    //RISCRead  : address 201201e0 data 00000410
    *((UINT32P) (0x20120100)) = 0x0000c70f;
    *((UINT32P) (0x20120140)) = 0x00008410;
    *((UINT32P) (0x201201c0)) = 0x00008800;
    *((UINT32P) (0x20120200)) = 0x00008800;
    *((UINT32P) (0x20120150)) = 0x00008810;
    *((UINT32P) (0x201201e0)) = 0x00008410;
    //RISCRead  : address 2012003c data 00000111
    *((UINT32P) (0x2012003c)) = 0x00000113;
    //RISCRead  : address 2012003c data 00000113
    *((UINT32P) (0x2012003c)) = 0x00000133;
    //RISCRead  : address 2012003c data 00000133
    *((UINT32P) (0x2012003c)) = 0x00005133;
    //RISCRead  : address 20120100 data 0000c70f
    *((UINT32P) (0x20120100)) = 0x0000470f;
    //RISCRead  : address 20120110 data 0000xX03
    *((UINT32P) (0x20120110)) = 0x00000002;
    //RISCRead  : address 20120100 data 0000470f
    *((UINT32P) (0x20120100)) = 0x0000c70f;
    *((UINT32P) (0x2012008c)) = 0x0000c100;
    //RISCRead  : address 2000045c data 01008510
    *((UINT32P) (0x2000045c)) = 0x21008510;
    *((UINT32P) (0x20120060)) = 0x00002020;
    *((UINT32P) (0x20120064)) = 0x00002000;
    *((UINT32P) (0x20120068)) = 0x00006f40;
    *((UINT32P) (0x20120094)) = 0x00004500;
    *((UINT32P) (0x20120098)) = 0x00000020;
    *((UINT32P) (0x2012009c)) = 0x00000003;
    //RISCRead  : address 20000458 data 00000000
    *((UINT32P) (0x20000458)) = 0xfff50142;
    //---------------------End of setup PLL for MDSYS
    //MD2GSYS_clock switch
    *((UINT32P) (0x22c00040)) = 0x41f041f0;
}
    dbgmsg0(CRITICAL, "---- disable M4U DCM\n");
    //*((UINT32P) (0x10001044)) = 0x00000100;
    //dprintf(CRITICAL, "0x10214050 = 0x%08X\n", *((UINT32P) (0x10214050)));
    *((UINT32P) (0x10214050)) = 0x0000007f;
    //dprintf(CRITICAL, "0x10205050 = 0x%08X\n", *((UINT32P) (0x10205050)));
    *((UINT32P) (0x10205050)) = 0x0000007f;
    //RISCRead  : address 10203060 data 00000000
    //dprintf(CRITICAL, "0x10203060 = 0x%08X\n", *((UINT32P) (0x10203060)));
    rdata = *((UINT32P) (0x10203060));
    rdata |= 0x40000000;
    *((UINT32P) (0x10203060)) = rdata;
    //dprintf(CRITICAL, "0x11000070 = 0x%08X\n", *((UINT32P) (0x11000070)));    
    rstd_reg = *((UINT32P) (0x11000070));
    *((UINT32P) (0x11000070)) = 0x00000000;
    dbgmsg0(CRITICAL, "---- turn on audio clock\n");
    *((UINT32P) (0x11220000)) = 0x803d4038;
    //dprintf(CRITICAL, "0x1128079c = 0x%08X\n", *((UINT32P) (0x1128079c)));
    *((UINT32P) (0x1128079c)) = 0x00000002;
    //dprintf(CRITICAL, "0x11280700 = 0x%08X\n", *((UINT32P) (0x11280700)));
    *((UINT32P) (0x11280700)) = 0x00000000;
    dbgmsg0(CRITICAL, "---- turn on mmsys clock\n");    //wx ok
    //*((UINT32P) (0x10000048)) = 0x80000000;
    *((UINT32P) (0x14000108)) = 0xffffffff;
    *((UINT32P) (0x14000118)) = 0xffffffff;
    dbgmsg0(CRITICAL, "---- turn on mjc clock\n");
    *((UINT32P) (0x17000000)) = 0x00000000;
    *((UINT32P) (0x17000010)) = 0x00000007;
    *((UINT32P) (0x17000020)) = 0x00000000;
    dbgmsg0(CRITICAL, "---- turn on imgsys clock\n");
    *((UINT32P) (0x15000000)) = 0x00000000;
    *((UINT32P) (0x15000008)) = 0xffffffff;
    *((UINT32P) (0x15000098)) = 0x0000000f;
    *((UINT32P) (0x15004170)) = 0xffffffff;
    //img DCM disable
    *((UINT32P) (0x15004188)) = 0x03ffffff;
    *((UINT32P) (0x1500418c)) = 0x024ecfe8;
    *((UINT32P) (0x15004190)) = 0x07ffffff;
    *((UINT32P) (0x15004194)) = 0x0000007f;
    *((UINT32P) (0x15004198)) = 0x000fffff;
    dbgmsg0(CRITICAL, "---- turn on vdec clock\n");
    *((UINT32P) (0x16000000)) = 0x00000001;
    *((UINT32P) (0x160200f4)) = 0x00000000;
    dbgmsg0(CRITICAL, "---- turn on venc clock\n"); // disable venc dcm
    *((UINT32P) (0x18000004)) = 0x00001111;
    *((UINT32P) (0x180020ec)) = 0x00000001;
    *((UINT32P) (0x180020f4)) = 0x00000000;
    *((UINT32P) (0x180020fc)) = 0x00000000;
    *((UINT32P) (0x18003300)) = 0x00000001;
    *((UINT32P) (0x18003400)) = 0x00000001;
    *((UINT32P) (0x18004300)) = 0x00000001;
    dbgmsg0(CRITICAL, "---- wait for MTCMOS power ready\n");
    //RISCRead  : address 23010820 data 1f010001    // MODEM2G_TOPSM_RM_PWR_STA
    //RISCRead  : address 23010820 data 1f010001      // MODEM2G_TOPSM_RM_PWR_STA
    //RISCRead  : address 23010820 data 1f010001      // MODEM2G_TOPSM_RM_PWR_STA
    //RISCRead  : address 23010820 data 1f010001      // MODEM2G_TOPSM_RM_PWR_STA
    //RISCRead  : address 27010820 data 1f1e001e
    //RISCRead  : address 27010820 data 1f1e001e
    //RISCRead  : address 27010820 data 1f1e001e
    //RISCRead  : address 27010820 data 1f1e001e
if(!bypass_md)
{
    dbgmsg0(CRITICAL, "---- turn on mdsys clock\n");
    //RISCRead  : address 80000458 data 00000000
    *((UINT32P) (0x20000458)) = 0xffffffff;
    //RISCRead  : address 2000045c data 21008510
    *((UINT32P) (0x2000045c)) = 0x21008510;
    //RISCRead  : address 2012009c data 00000003
    *((UINT32P) (0x2012009c)) = 0x00000003;
    *((UINT32P) (0x23000010)) = 0xffffffff;
    *((UINT32P) (0x27000010)) = 0xffffffff;
    *((UINT32P) (0x23000018)) = 0xfffffffe;
    //RISCRead  : address 23000028 data 00000001
    *((UINT32P) (0x23000028)) = 0x00000001;
    *((UINT32P) (0x27000018)) = 0xffffffff;
    dbgmsg0(CRITICAL, "---- turn on modemsys clock\n");
    *((UINT32P) (0x23000098)) = 0xffffffff;
    *((UINT32P) (0x27000098)) = 0xffffffff;
    dbgmsg0(CRITICAL, "---- turn on md2gsys clock\n");
    *((UINT32P) (0x22c00028)) = 0xffffffff;
    dbgmsg0(CRITICAL, "---- turn on hspa1 clock\n");
    *((UINT32P) (0x27200078)) = 0xffffffff;
    *((UINT32P) (0x27200030)) = 0x00000000;
    *((UINT32P) (0x27400078)) = 0xffffffff;
    *((UINT32P) (0x27400030)) = 0x00000000;
    *((UINT32P) (0x2767002c)) = 0xffffffff;
    *((UINT32P) (0x27670010)) = 0x00000000;
    *((UINT32P) (0x27870040)) = 0xffffffff;
    *((UINT32P) (0x27870028)) = 0x00000000;
    dbgmsg0(CRITICAL, "---- turn on tdd clock\n");
    *((UINT16P) (0x24000422)) = 0x4911;
    *((UINT16P) (0x24000422)) = 0xc911;
}  
    // MIPI DSI0/1 --> CSI0/1 Config -------------------------
    //RISCRead  : address 10215044 data 88492480
    *((UINT32P) (0x10215044)) = 0x88492493;
    //RISCRead  : address 10216044 data 88492480
    *((UINT32P) (0x10216044)) = 0x88492493;
    //RISCRead  : address 10215040 data 00000080
    *((UINT32P) (0x10215040)) = 0x00000082;
    //RISCRead  : address 10216040 data 00000080
    *((UINT32P) (0x10216040)) = 0x00000082;
    //RISCRead  : address 10215000 data 00000400
    *((UINT32P) (0x10215000)) = 0x00000403;
    //RISCRead  : address 10216000 data 00000400
    *((UINT32P) (0x10216000)) = 0x00000403;
    *((UINT32P) (0x10215068)) = 0x00000003;
    *((UINT32P) (0x10215068)) = 0x00000001;
    *((UINT32P) (0x10216068)) = 0x00000003;
    *((UINT32P) (0x10216068)) = 0x00000001;
    //RISCRead  : address 10215044 data 88492493
    *((UINT32P) (0x10215044)) = 0x88492483;
    //RISCRead  : address 10216044 data 88492493
    *((UINT32P) (0x10216044)) = 0x88492483;
    *((UINT32P) (0x10215050)) = 0x00000000;
    *((UINT32P) (0x10216050)) = 0x00000000;
    *((UINT32P) (0x10215054)) = 0x00000003;
    *((UINT32P) (0x10216054)) = 0x00000003;
    *((UINT32P) (0x10215058)) = 0x78000000;
    *((UINT32P) (0x10216058)) = 0x78000000;
    *((UINT32P) (0x1021505c)) = 0x00000000;
    *((UINT32P) (0x1021605c)) = 0x00000000;
    //RISCRead  : address 10215050 data 00000000
    *((UINT32P) (0x10215050)) = 0x00000001;
    //RISCRead  : address 10216050 data 00000000
    *((UINT32P) (0x10216050)) = 0x00000001;
    //END  MIPI DSI0/1 --> CSI0/1 Config -------------------------
#endif
#if 1
    //==================================================
    //MBIST main setting
    //==================================================
    dbgmsg0(CRITICAL, "---- Reset reg_load_fuse\n");
    //=== 8) reset reg_load_fuse
    *((UINT32P) (0x100011a8)) = 0x000f0000;
if(!bypass_md)
{
    *((UINT32P) (0x20110074)) = 0x00000007;
    *((UINT32P) (0x22c10090)) = 0x000000ff;
    *((UINT32P) (0x27440068)) = 0x00000001;
    *((UINT32P) (0x2744006c)) = 0x00000001;
    *((UINT32P) (0x276e00b0)) = 0x00000001;
    *((UINT32P) (0x278b0060)) = 0x00000001;
    *((UINT32P) (0x278b0064)) = 0x00000001;
    *((UINT32P) (0x278b0068)) = 0x00000001;
    *((UINT32P) (0x26612028)) = 0x00000004;
    *((UINT32P) (0x2660202c)) = 0x00000004;
    *((UINT32P) (0x26632024)) = 0x00000004;
    *((UINT32P) (0x26622040)) = 0x00000004;
    *((UINT32P) (0x26642028)) = 0x00000004;
    *((UINT32P) (0x26652028)) = 0x00000004;
    *((UINT32P) (0x26612028)) = 0x00000000;
    *((UINT32P) (0x2660202c)) = 0x00000000;
    *((UINT32P) (0x26632024)) = 0x00000000;
    *((UINT32P) (0x26622040)) = 0x00000000;
    *((UINT32P) (0x26642028)) = 0x00000000;
    *((UINT32P) (0x26652028)) = 0x00000000;
}
    __asm__ __volatile__ ("dsb" : : : "memory");
    *((UINT32P) (0x100011a8)) = 0x00000000;
if(!bypass_md)
{
    *((UINT32P) (0x20110074)) = 0x00000000;
    *((UINT32P) (0x22c10090)) = 0x00000000;
    *((UINT32P) (0x27440068)) = 0x00000000;
    *((UINT32P) (0x2744006c)) = 0x00000000;
    *((UINT32P) (0x276e00b0)) = 0x00000000;
    *((UINT32P) (0x278b0060)) = 0x00000000;
    *((UINT32P) (0x278b0064)) = 0x00000000;
    *((UINT32P) (0x278b0068)) = 0x00000000;
    *((UINT32P) (0x26612028)) = 0x00000000;
    *((UINT32P) (0x2660202c)) = 0x00000000;
    *((UINT32P) (0x26632024)) = 0x00000000;
    *((UINT32P) (0x26622040)) = 0x00000000;
    *((UINT32P) (0x26642028)) = 0x00000000;
    *((UINT32P) (0x26652028)) = 0x00000000;
    *((UINT32P) (0x26612028)) = 0x00000000;
    *((UINT32P) (0x2660202c)) = 0x00000000;
    *((UINT32P) (0x26632024)) = 0x00000000;
    *((UINT32P) (0x26622040)) = 0x00000000;
    *((UINT32P) (0x26642028)) = 0x00000000;
    *((UINT32P) (0x26652028)) = 0x00000000;
}
    dbgmsg0(CRITICAL, "---- MBIST main setting\n");
    //write mml1/LTE background
if(!bypass_md)
{
    *((UINT32P) (0x2670600c)) = 0x00000000;
    *((UINT32P) (0x2673600c)) = 0x00000000;
    *((UINT32P) (0x26716008)) = 0x00000000;
    *((UINT32P) (0x26726010)) = 0x00000000;
    *((UINT32P) (0x26746008)) = 0x00000000;
    *((UINT32P) (0x26756008)) = 0x00000000;
}
    *((UINT32P) (0x1020d028)) = 0xb34872bf;
    *((UINT32P) (0x1020d030)) = 0x2f5e465a;
    *((UINT32P) (0x1020d034)) = 0x323bcf72;
    dbgmsg1(CRITICAL, "@@@ [10]\n");
    *((UINT32P) (0x1020d038)) = 0xcc054589;
    *((UINT32P) (0x1020d03c)) = 0xff8a4dd9;
if(!bypass_md)
{
    *((UINT32P) (0x24050038)) = 0xa4711df5;
    *((UINT32P) (0x2405003c)) = 0x09e90163;
    *((UINT32P) (0x24050040)) = 0xfb3a8db8;
    *((UINT32P) (0x24050044)) = 0x2d3daecd;
    *((UINT32P) (0x24050048)) = 0xa4711df5;
    *((UINT32P) (0x2405004c)) = 0x99236f84;
    *((UINT32P) (0x24050050)) = 0xf83e5e25;
    *((UINT32P) (0x24050054)) = 0x1da83a8f;
    dbgmsg1(CRITICAL, "@@@ [20]\n");
    *((UINT32P) (0x24050058)) = 0xf83e5e25;
    *((UINT32P) (0x2405005c)) = 0x1da83a8f;
    *((UINT32P) (0x26500000)) = 0x00000005;
    *((UINT32P) (0x26510000)) = 0x00000005;
    *((UINT32P) (0x26520000)) = 0x00000005;
    *((UINT32P) (0x26530000)) = 0x00000005;
    *((UINT32P) (0x26540000)) = 0x00000005;
    *((UINT32P) (0x26550000)) = 0x00000005;
    *((UINT32P) (0x26501000)) = 0x00000006;
    *((UINT32P) (0x26531000)) = 0x00000006;
    dbgmsg1(CRITICAL, "@@@ [30]\n");
    *((UINT32P) (0x26521000)) = 0x00000006;
    *((UINT32P) (0x26541000)) = 0x00000006;
    *((UINT32P) (0x26551000)) = 0x00000006;
    *((UINT32P) (0x26511000)) = 0x00000006;
    *((UINT32P) (0x26500004)) = 0x00000020;
    *((UINT32P) (0x26500008)) = 0x00000002;
    *((UINT32P) (0x26500000)) = 0xffff000b;
    *((UINT32P) (0x26200000)) = 0x00000000;
    *((UINT32P) (0x26200004)) = 0x00000000;
    *((UINT32P) (0x26180000)) = 0x00018000;
    dbgmsg1(CRITICAL, "@@@ [40]\n");
    *((UINT32P) (0x26180004)) = 0x00019300;
    *((UINT32P) (0x26180008)) = 0x0004c080;
    *((UINT32P) (0x2618000c)) = 0x0000800f;
    *((UINT32P) (0x26180010)) = 0x0002de00;
    *((UINT32P) (0x26180014)) = 0x0001300f;
    *((UINT32P) (0x26180018)) = 0x00000000;
    *((UINT32P) (0x2618001c)) = 0x00000000;
    *((UINT32P) (0x26180020)) = 0x00000000;
    *((UINT32P) (0x26180024)) = 0x00000000;
    *((UINT32P) (0x26180028)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [50]\n");
    *((UINT32P) (0x2618002c)) = 0x00000000;
    *((UINT32P) (0x26180030)) = 0x00000000;
    *((UINT32P) (0x26180034)) = 0x00000000;
    *((UINT32P) (0x26180038)) = 0x00000000;
    *((UINT32P) (0x2618003c)) = 0x00000000;
    *((UINT32P) (0x26180040)) = 0x00000000;
    *((UINT32P) (0x26180044)) = 0x00000000;
    *((UINT32P) (0x26180048)) = 0x00000000;
    *((UINT32P) (0x2618004c)) = 0x00000000;
    *((UINT32P) (0x26180050)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [60]\n");
    *((UINT32P) (0x26180054)) = 0x00000000;
    *((UINT32P) (0x26180058)) = 0x00000000;
    *((UINT32P) (0x2618005c)) = 0x00000000;
    *((UINT32P) (0x26180060)) = 0x00000000;
    *((UINT32P) (0x26180064)) = 0x00000000;
    *((UINT32P) (0x26180068)) = 0x00000000;
    *((UINT32P) (0x2618006c)) = 0x00000000;
    *((UINT32P) (0x26180070)) = 0x001983f2;
    *((UINT32P) (0x26180074)) = 0x00000000;
    *((UINT32P) (0x26180078)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [70]\n");
    *((UINT32P) (0x2618007c)) = 0x00000000;
    *((UINT32P) (0x26530004)) = 0x00000020;
    *((UINT32P) (0x26530008)) = 0x00000002;
    *((UINT32P) (0x26530000)) = 0xffff000b;
    *((UINT32P) (0x26260000)) = 0x00000000;
    *((UINT32P) (0x26260004)) = 0x00000000;
    *((UINT32P) (0x260c0000)) = 0x00018000;
    *((UINT32P) (0x260c0004)) = 0x00019300;
    *((UINT32P) (0x260c0008)) = 0x0004c080;
    *((UINT32P) (0x260c000c)) = 0x0000800f;
    dbgmsg1(CRITICAL, "@@@ [80]\n");
    *((UINT32P) (0x260c0010)) = 0x0002de00;
    *((UINT32P) (0x260c0014)) = 0x0001300f;
    *((UINT32P) (0x260c0018)) = 0x00000000;
    *((UINT32P) (0x260c001c)) = 0x00000000;
    *((UINT32P) (0x260c0020)) = 0x00000000;
    *((UINT32P) (0x260c0024)) = 0x00000000;
    *((UINT32P) (0x260c0028)) = 0x00000000;
    *((UINT32P) (0x260c002c)) = 0x00000000;
    *((UINT32P) (0x260c0030)) = 0x00000000;
    *((UINT32P) (0x260c0034)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [90]\n");
    *((UINT32P) (0x260c0038)) = 0x00000000;
    *((UINT32P) (0x260c003c)) = 0x00000000;
    *((UINT32P) (0x260c0040)) = 0x00000000;
    *((UINT32P) (0x260c0044)) = 0x00000000;
    *((UINT32P) (0x260c0048)) = 0x00000000;
    *((UINT32P) (0x260c004c)) = 0x00000000;
    *((UINT32P) (0x260c0050)) = 0x00000000;
    *((UINT32P) (0x260c0054)) = 0x00000000;
    *((UINT32P) (0x260c0058)) = 0x00000000;
    *((UINT32P) (0x260c005c)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [100]\n");
    *((UINT32P) (0x260c0060)) = 0x00000000;
    *((UINT32P) (0x260c0064)) = 0x00000000;
    *((UINT32P) (0x260c0068)) = 0x00000000;
    *((UINT32P) (0x260c006c)) = 0x00000000;
    *((UINT32P) (0x260c0070)) = 0x001983f2;
    *((UINT32P) (0x260c0074)) = 0x00000000;
    *((UINT32P) (0x260c0078)) = 0x00000000;
    *((UINT32P) (0x260c007c)) = 0x00000000;
    *((UINT32P) (0x26520004)) = 0x00000020;
    *((UINT32P) (0x26520008)) = 0x00000002;
    dbgmsg1(CRITICAL, "@@@ [110]\n");
    *((UINT32P) (0x26520000)) = 0xffff000b;
    *((UINT32P) (0x26240000)) = 0x00000000;
    *((UINT32P) (0x26240004)) = 0x00000000;
    *((UINT32P) (0x26080000)) = 0x00018000;
    *((UINT32P) (0x26080004)) = 0x00019300;
    *((UINT32P) (0x26080008)) = 0x0004c080;
    *((UINT32P) (0x2608000c)) = 0x0000800f;
    *((UINT32P) (0x26080010)) = 0x0002de00;
    *((UINT32P) (0x26080014)) = 0x0001300f;
    *((UINT32P) (0x26080018)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [120]\n");
    *((UINT32P) (0x2608001c)) = 0x00000000;
    *((UINT32P) (0x26080020)) = 0x00000000;
    *((UINT32P) (0x26080024)) = 0x00000000;
    *((UINT32P) (0x26080028)) = 0x00000000;
    *((UINT32P) (0x2608002c)) = 0x00000000;
    *((UINT32P) (0x26080030)) = 0x00000000;
    *((UINT32P) (0x26080034)) = 0x00000000;
    *((UINT32P) (0x26080038)) = 0x00000000;
    *((UINT32P) (0x2608003c)) = 0x00000000;
    *((UINT32P) (0x26080040)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [130]\n");
    *((UINT32P) (0x26080044)) = 0x00000000;
    *((UINT32P) (0x26080048)) = 0x00000000;
    *((UINT32P) (0x2608004c)) = 0x00000000;
    *((UINT32P) (0x26080050)) = 0x00000000;
    *((UINT32P) (0x26080054)) = 0x00000000;
    *((UINT32P) (0x26080058)) = 0x00000000;
    *((UINT32P) (0x2608005c)) = 0x00000000;
    *((UINT32P) (0x26080060)) = 0x00000000;
    *((UINT32P) (0x26080064)) = 0x00000000;
    *((UINT32P) (0x26080068)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [140]\n");
    *((UINT32P) (0x2608006c)) = 0x00000000;
    *((UINT32P) (0x26080070)) = 0x001983f2;
    *((UINT32P) (0x26080074)) = 0x00000000;
    *((UINT32P) (0x26080078)) = 0x00000000;
    *((UINT32P) (0x2608007c)) = 0x00000000;
    *((UINT32P) (0x26540004)) = 0x00000020;
    *((UINT32P) (0x26540008)) = 0x00000002;
    *((UINT32P) (0x26540000)) = 0xffff000b;
    *((UINT32P) (0x26280000)) = 0x00000000;
    *((UINT32P) (0x26280004)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [150]\n");
    *((UINT32P) (0x26100000)) = 0x00018000;
    *((UINT32P) (0x26100004)) = 0x00019300;
    *((UINT32P) (0x26100008)) = 0x0004c080;
    *((UINT32P) (0x2610000c)) = 0x0000800f;
    *((UINT32P) (0x26100010)) = 0x0002de00;
    *((UINT32P) (0x26100014)) = 0x0001300f;
    *((UINT32P) (0x26100018)) = 0x00000000;
    *((UINT32P) (0x2610001c)) = 0x00000000;
    *((UINT32P) (0x26100020)) = 0x00000000;
    *((UINT32P) (0x26100024)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [160]\n");
    *((UINT32P) (0x26100028)) = 0x00000000;
    *((UINT32P) (0x2610002c)) = 0x00000000;
    *((UINT32P) (0x26100030)) = 0x00000000;
    *((UINT32P) (0x26100034)) = 0x00000000;
    *((UINT32P) (0x26100038)) = 0x00000000;
    *((UINT32P) (0x2610003c)) = 0x00000000;
    *((UINT32P) (0x26100040)) = 0x00000000;
    *((UINT32P) (0x26100044)) = 0x00000000;
    *((UINT32P) (0x26100048)) = 0x00000000;
    *((UINT32P) (0x2610004c)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [170]\n");
    *((UINT32P) (0x26100050)) = 0x00000000;
    *((UINT32P) (0x26100054)) = 0x00000000;
    *((UINT32P) (0x26100058)) = 0x00000000;
    *((UINT32P) (0x2610005c)) = 0x00000000;
    *((UINT32P) (0x26100060)) = 0x00000000;
    *((UINT32P) (0x26100064)) = 0x00000000;
    *((UINT32P) (0x26100068)) = 0x00000000;
    *((UINT32P) (0x2610006c)) = 0x00000000;
    *((UINT32P) (0x26100070)) = 0x001983f2;
    *((UINT32P) (0x26100074)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [180]\n");
    *((UINT32P) (0x26100078)) = 0x00000000;
    *((UINT32P) (0x2610007c)) = 0x00000000;
    *((UINT32P) (0x26550004)) = 0x00000020;
    *((UINT32P) (0x26550008)) = 0x00000002;
    *((UINT32P) (0x26550000)) = 0xffff000b;
    *((UINT32P) (0x262a0000)) = 0x00000000;
    *((UINT32P) (0x262a0004)) = 0x00000000;
    *((UINT32P) (0x26140000)) = 0x00018000;
    *((UINT32P) (0x26140004)) = 0x00019300;
    *((UINT32P) (0x26140008)) = 0x0004c080;
    dbgmsg1(CRITICAL, "@@@ [190]\n");
    *((UINT32P) (0x2614000c)) = 0x0000800f;
    *((UINT32P) (0x26140010)) = 0x0002de00;
    *((UINT32P) (0x26140014)) = 0x0001300f;
    *((UINT32P) (0x26140018)) = 0x00000000;
    *((UINT32P) (0x2614001c)) = 0x00000000;
    *((UINT32P) (0x26140020)) = 0x00000000;
    *((UINT32P) (0x26140024)) = 0x00000000;
    *((UINT32P) (0x26140028)) = 0x00000000;
    *((UINT32P) (0x2614002c)) = 0x00000000;
    *((UINT32P) (0x26140030)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [200]\n");
    *((UINT32P) (0x26140034)) = 0x00000000;
    *((UINT32P) (0x26140038)) = 0x00000000;
    *((UINT32P) (0x2614003c)) = 0x00000000;
    *((UINT32P) (0x26140040)) = 0x00000000;
    *((UINT32P) (0x26140044)) = 0x00000000;
    *((UINT32P) (0x26140048)) = 0x00000000;
    *((UINT32P) (0x2614004c)) = 0x00000000;
    *((UINT32P) (0x26140050)) = 0x00000000;
    *((UINT32P) (0x26140054)) = 0x00000000;
    *((UINT32P) (0x26140058)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [210]\n");
    *((UINT32P) (0x2614005c)) = 0x00000000;
    *((UINT32P) (0x26140060)) = 0x00000000;
    *((UINT32P) (0x26140064)) = 0x00000000;
    *((UINT32P) (0x26140068)) = 0x00000000;
    *((UINT32P) (0x2614006c)) = 0x00000000;
    *((UINT32P) (0x26140070)) = 0x001983f2;
    *((UINT32P) (0x26140074)) = 0x00000000;
    *((UINT32P) (0x26140078)) = 0x00000000;
    *((UINT32P) (0x2614007c)) = 0x00000000;
    *((UINT32P) (0x26510004)) = 0x00000020;
    dbgmsg1(CRITICAL, "@@@ [220]\n");
    *((UINT32P) (0x26510008)) = 0x00000002;
    *((UINT32P) (0x26510000)) = 0xffff000b;
    *((UINT32P) (0x26220000)) = 0x00000000;
    *((UINT32P) (0x26220004)) = 0x00000000;
    *((UINT32P) (0x26040000)) = 0x00018000;
    *((UINT32P) (0x26040004)) = 0x00019300;
    *((UINT32P) (0x26040008)) = 0x0004c080;
    *((UINT32P) (0x2604000c)) = 0x0000800f;
    *((UINT32P) (0x26040010)) = 0x0002de00;
    *((UINT32P) (0x26040014)) = 0x0001300f;
    dbgmsg1(CRITICAL, "@@@ [230]\n");
    *((UINT32P) (0x26040018)) = 0x00000000;
    *((UINT32P) (0x2604001c)) = 0x00000000;
    *((UINT32P) (0x26040020)) = 0x00000000;
    *((UINT32P) (0x26040024)) = 0x00000000;
    *((UINT32P) (0x26040028)) = 0x00000000;
    *((UINT32P) (0x2604002c)) = 0x00000000;
    *((UINT32P) (0x26040030)) = 0x00000000;
    *((UINT32P) (0x26040034)) = 0x00000000;
    *((UINT32P) (0x26040038)) = 0x00000000;
    *((UINT32P) (0x2604003c)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [240]\n");
    *((UINT32P) (0x26040040)) = 0x00000000;
    *((UINT32P) (0x26040044)) = 0x00000000;
    *((UINT32P) (0x26040048)) = 0x00000000;
    *((UINT32P) (0x2604004c)) = 0x00000000;
    *((UINT32P) (0x26040050)) = 0x00000000;
    *((UINT32P) (0x26040054)) = 0x00000000;
    *((UINT32P) (0x26040058)) = 0x00000000;
    *((UINT32P) (0x2604005c)) = 0x00000000;
    *((UINT32P) (0x26040060)) = 0x00000000;
    *((UINT32P) (0x26040064)) = 0x00000000;
    dbgmsg1(CRITICAL, "@@@ [250]\n");
    *((UINT32P) (0x26040068)) = 0x00000000;
    *((UINT32P) (0x2604006c)) = 0x00000000;
    *((UINT32P) (0x26040070)) = 0x001983f2;
    *((UINT32P) (0x26040074)) = 0x00000000;
    *((UINT32P) (0x26040078)) = 0x00000000;
    *((UINT32P) (0x2604007c)) = 0x00000000;
    *((UINT32P) (0x26721024)) = 0x0aa5a42c;
    *((UINT32P) (0x26721028)) = 0x5cc48a28;
    *((UINT32P) (0x2672102c)) = 0x69717c03;
    *((UINT32P) (0x26721030)) = 0x05b65436;
    dbgmsg1(CRITICAL, "@@@ [260]\n");
    *((UINT32P) (0x26721034)) = 0x9bb9f281;
    *((UINT32P) (0x26721038)) = 0xdf4075c9;
    *((UINT32P) (0x2672103c)) = 0x0373f1d7;
    *((UINT32P) (0x26721040)) = 0x60332043;
    *((UINT32P) (0x26721054)) = 0x151a61ae;
    *((UINT32P) (0x26721058)) = 0x4a01935c;
    *((UINT32P) (0x2672105c)) = 0x4e3482f0;
    *((UINT32P) (0x26721060)) = 0xfd931450;
    *((UINT32P) (0x26341160)) = 0x00000000;
    *((UINT32P) (0x26341164)) = 0x000049ce;
    dbgmsg1(CRITICAL, "@@@ [270]\n");
    *((UINT32P) (0x26341160)) = 0x00000001;
    *((UINT32P) (0x26341164)) = 0x000048b1;
    *((UINT32P) (0x26341160)) = 0x00000002;
    *((UINT32P) (0x26341164)) = 0x0000e523;
    *((UINT32P) (0x26341160)) = 0x00000003;
    *((UINT32P) (0x26341164)) = 0x00005f75;
    *((UINT32P) (0x26341160)) = 0x00000004;
    *((UINT32P) (0x26341164)) = 0x00002808;
    *((UINT32P) (0x26341160)) = 0x00000005;
    *((UINT32P) (0x26341164)) = 0x00003adb;
    dbgmsg1(CRITICAL, "@@@ [280]\n");
    *((UINT32P) (0x26341160)) = 0x00000006;
    *((UINT32P) (0x26341164)) = 0x0000f084;
    *((UINT32P) (0x26341160)) = 0x00000007;
    *((UINT32P) (0x26341164)) = 0x00004949;
    *((UINT32P) (0x26341160)) = 0x00000008;
    *((UINT32P) (0x26341164)) = 0x00009fea;
    *((UINT32P) (0x26341160)) = 0x00000009;
    *((UINT32P) (0x26341164)) = 0x0000f5f5;
    *((UINT32P) (0x26341160)) = 0x0000000a;
    *((UINT32P) (0x26341164)) = 0x00002fc7;
    dbgmsg1(CRITICAL, "@@@ [290]\n");
    *((UINT32P) (0x26341160)) = 0x0000000b;
    *((UINT32P) (0x26341164)) = 0x0000ba07;
    *((UINT32P) (0x26300ba4)) = 0x00000006;
    *((UINT32P) (0x26300ba8)) = 0x0000a4c1;
    *((UINT32P) (0x26300ba8)) = 0x00005c78;
    *((UINT32P) (0x26300ba8)) = 0x0000a4c1;
    *((UINT32P) (0x26300ba8)) = 0x00005c78;
    *((UINT32P) (0x26300ba8)) = 0x0000a4c1;
    *((UINT32P) (0x26300ba8)) = 0x00005c78;
    *((UINT32P) (0x26300ba8)) = 0x0000a4c1;
    dbgmsg1(CRITICAL, "@@@ [300]\n");
    *((UINT32P) (0x26300ba8)) = 0x00005c78;
    *((UINT32P) (0x26300ba8)) = 0x0000a4c1;
    *((UINT32P) (0x26300ba8)) = 0x00005c78;
    *((UINT32P) (0x26300ba8)) = 0x0000a4c1;
    *((UINT32P) (0x26300ba8)) = 0x00005c78;
    dbgmsg0(CRITICAL, "---- set md done/fail mask\n");
}
    //RISCRead  : address 20110018 data 0007ffff
    //RISCRead  : address 2011001c data ffffffff
    //RISCRead  : address 20110020 data ffffffff
    //RISCRead  : address 20110024 data 01ffffff
    //RISCRead  : address 20110028 data 0000007f
    //RISCRead  : address 201d8018 data 00000003
    //RISCRead  : address 201d801c data 000007ff
    //RISCRead  : address 2504001c data 0001ffff
    //RISCRead  : address 25040020 data 0001ffff
    //RISCRead  : address 2020e018 data 00000007
    //RISCRead  : address 2020e01c data 00000007
    //RISCRead  : address 2300801c data 0000007f
    //RISCRead  : address 23008020 data 000001ff
    //RISCRead  : address 2700801c data 0000007f
    //RISCRead  : address 27008020 data 000001ff
    //RISCRead  : address 22c10018 data 7fffffff
    //RISCRead  : address 22c1001c data 7fffffff
    //RISCRead  : address 22c10020 data 0fffffff
    //RISCRead  : address 27230018 data 0000003f
    //RISCRead  : address 2723001c data ffffffff
    //RISCRead  : address 27230020 data 00000003
    //RISCRead  : address 27440018 data 000000ff
    //RISCRead  : address 2744001c data ffffffff
    //RISCRead  : address 27440020 data 00003fff
    //RISCRead  : address 276e0018 data 0007ffff
    //RISCRead  : address 276e001c data ffffffff
    //RISCRead  : address 276e0020 data ffffffff
    //RISCRead  : address 276e0024 data ffffffff
    //RISCRead  : address 276e0028 data ffffffff
    //RISCRead  : address 276e002c data 0000000f
    //RISCRead  : address 278b0018 data 7fffffff
    //RISCRead  : address 278b001c data 001fffff
    //RISCRead  : address 278b0020 data 7fffffff
    //RISCRead  : address 278b0024 data 001fffff
    *((UINT32P) (0x100062c8)) = 0x00000ff0;
    //=== 4) reset MBIST
    *((UINT32P) (0x10210000)) = 0x0000001d;
    *((UINT32P) (0x10210000)) = 0x0000001f;

    *((UINT32P) (0x100011a8)) = 0x00000000;
    *((UINT32P) (0x1020d050)) = 0x00000000;
    *((UINT32P) (0x14000800)) = 0x00000000;
    *((UINT32P) (0x140008a0)) = 0x00000000;
    *((UINT32P) (0x17000050)) = 0x00000000;
    *((UINT32P) (0x17000040)) = 0x00000000;
    *((UINT32P) (0x18000040)) = 0x00000000;
    *((UINT32P) (0x1800005c)) = 0x00000000;
    *((UINT32P) (0x15000060)) = 0x00000000;
    *((UINT32P) (0x15000078)) = 0x00000000;
    *((UINT32P) (0x1600002c)) = 0x00000000;
    *((UINT32P) (0x160000c8)) = 0x00000000;
if(!bypass_md)
{
    *((UINT32P) (0x20110074)) = 0x00000000;
    *((UINT32P) (0x201d8030)) = 0x00000000;
    *((UINT32P) (0x25040040)) = 0x00000000;
    *((UINT32P) (0x2020e028)) = 0x00000000;
    *((UINT32P) (0x23008040)) = 0x00000000;
    *((UINT32P) (0x27008040)) = 0x00000000;
    *((UINT32P) (0x22c10050)) = 0x00000000;
    *((UINT32P) (0x22c1006c)) = 0x00000000;
    *((UINT32P) (0x27230044)) = 0x00000000;
    *((UINT32P) (0x27440048)) = 0x00000000;
    *((UINT32P) (0x27440070)) = 0x00000000;
    *((UINT32P) (0x276e008c)) = 0x00000000;
    *((UINT32P) (0x276e009c)) = 0x00000000;
    *((UINT32P) (0x278b0050)) = 0x00000000;
    *((UINT32P) (0x278b0064)) = 0x00000000;
    *((UINT32P) (0x24050068)) = 0x00000000;
}
    *((UINT32P) (0x13fff310)) = 0x00000000;
    *((UINT32P) (0x13fff314)) = 0x00000000;
    __asm__ __volatile__ ("dsb" : : : "memory");
    *((UINT32P) (0x100011a8)) = 0x00070000;
    *((UINT32P) (0x1020d050)) = 0x00010000;
    *((UINT32P) (0x14000800)) = 0x00008000;
    *((UINT32P) (0x140008a0)) = 0x00000001;
    *((UINT32P) (0x17000050)) = 0x00000001;
    *((UINT32P) (0x17000040)) = 0x00010000;
    *((UINT32P) (0x18000040)) = 0x00000001;
    *((UINT32P) (0x1800005c)) = 0x00000001;
    *((UINT32P) (0x1600002c)) = 0x00000001;
    *((UINT32P) (0x160000c8)) = 0x00000001;
    *((UINT32P) (0x15000060)) = 0x00000001;
    *((UINT32P) (0x15000078)) = 0x00000001;
if(!bypass_md)
{
    *((UINT32P) (0x20110074)) = 0x00000003;
    *((UINT32P) (0x201d8030)) = 0x00000001;
    *((UINT32P) (0x25040040)) = 0x00000001;
    *((UINT32P) (0x2020e028)) = 0x00000001;
    *((UINT32P) (0x23008040)) = 0x00000001;
    *((UINT32P) (0x27008040)) = 0x00000001;
    *((UINT32P) (0x22c10050)) = 0x00000001;
    *((UINT32P) (0x22c1006c)) = 0x00000001;
    *((UINT32P) (0x27230044)) = 0x00000001;
    *((UINT32P) (0x27440048)) = 0x00000001;
    *((UINT32P) (0x27440070)) = 0x00000001;
    *((UINT32P) (0x276e008c)) = 0x00000001;
    *((UINT32P) (0x276e009c)) = 0x00000001;
    *((UINT32P) (0x278b0050)) = 0x00000001;
    *((UINT32P) (0x278b0064)) = 0x00000001;
    *((UINT32P) (0x24050068)) = 0x00000001;
}
    *((UINT32P) (0x13fff310)) = 0x00000001;
    *((UINT32P) (0x13fff314)) = 0x00000001;
    *((UINT32P) (0x13fff00c)) = 0xffffffff;
    *((UINT32P) (0x13fff00c)) = 0x00000000;
    *((UINT32P) (0x13000a10)) = 0x048000b0;
    *((UINT32P) (0x13000a08)) = 0x05511111;
    *((UINT32P) (0x13000000)) = 0x55555555;
    *((UINT32P) (0x13000004)) = 0x55555555;
    *((UINT32P) (0x13000008)) = 0x55555555;
    *((UINT32P) (0x1300000c)) = 0x55555555;
    //=== 5) set mbist_mode = 1;
    //bypass infra    *((UINT32P) (0x100011b8)) = 0x00000003;
    *((UINT32P) (0x1020d060)) = 0x03ff0000; //audio only
    //bypass infra    *((UINT32P) (0x1020d064)) = 0xffffffff;
    *((UINT32P) (0x1400080c)) = 0x007fffff;
    *((UINT32P) (0x17000038)) = 0x000007ff;
    *((UINT32P) (0x15000064)) = 0xffffffff;
    *((UINT32P) (0x15000068)) = 0x00003fff;
    *((UINT32P) (0x16000028)) = 0xffffffff;
    *((UINT32P) (0x16000048)) = 0x0000001f;
    *((UINT32P) (0x1800000c)) = 0x0007ffff;
if(!bypass_md)
{
    *((UINT32P) (0x20110000)) = 0x0001ffff;
    *((UINT32P) (0x25040000)) = 0x000000ff;
    *((UINT32P) (0x2020e000)) = 0x00000007;
    *((UINT32P) (0x23008000)) = 0x000000fe;
    *((UINT32P) (0x27008000)) = 0x000000fe;
    *((UINT32P) (0x22c10000)) = 0x7fffffff;
    if (!hspa_off)
    {
        *((UINT32P) (0x27230000)) = 0x0000001f;
        *((UINT32P) (0x27440000)) = 0x0000007f;
        *((UINT32P) (0x276e0000)) = 0x00001fff;
        *((UINT32P) (0x278b0000)) = 0x0000ffff;
    }
    if (!tdd_off)
    {
        *((UINT32P) (0x24050000)) = 0xffffffff;
        *((UINT32P) (0x24050004)) = 0x0000000f;
    }
    //if (!lte_off)
    {
        *((UINT32P) (0x200004b0)) = 0x00000600;
    }
}
    *((UINT32P) (0x13fff1a0)) = 0xffffffff;
    *((UINT32P) (0x13fff1a4)) = 0xffffffff;
    *((UINT32P) (0x13fff1a8)) = 0xffffffff;
    *((UINT32P) (0x13fff1ac)) = 0xffffffff;
    *((UINT32P) (0x13fff1b0)) = 0xffffffff;
    *((UINT32P) (0x13fff1b4)) = 0xffffffff;
    *((UINT32P) (0x13fff1b8)) = 0xffffffff;
    *((UINT32P) (0x13fff1bc)) = 0xffffffff;
    *((UINT32P) (0x13fff1c0)) = 0xffffffff;
    *((UINT32P) (0x13fff1c4)) = 0xffffffff;
    *((UINT32P) (0x13fff1c8)) = 0xffffffff;
    *((UINT32P) (0x13fff1cc)) = 0xffffffff;
    *((UINT32P) (0x13fff1d0)) = 0xffffffff;
    *((UINT32P) (0x13fff1d4)) = 0xffffffff;
    *((UINT32P) (0x13fff1d8)) = 0xffffffff;
    *((UINT32P) (0x13fff1dc)) = 0xffffffff;
    *((UINT32P) (0x13fff1e0)) = 0xffffffff;
    *((UINT32P) (0x13fff1e4)) = 0xffffffff;
    *((UINT32P) (0x13fff1e8)) = 0xffffffff;
    *((UINT32P) (0x13fff1ec)) = 0xffffffff;
    //=== 6) wait mbist_done
    //ADD delay here !!!
    udelay(2000);
    //bypass infra    if ((*((UINT32P) (0x100011c0)) & 0x403fc07f) != 0x403fc07f) {
    //bypass infra        dprintf(CRITICAL, "mbist_done 1 is not ready\n");
    //bypass infra        ret = -1;
    //bypass infra    }
    if ((*((UINT32P) (0x1020d068)) & 0x03ff0000) != 0x03ff0000) {
        dprintf(CRITICAL, "mbist_done 2 is not ready\n");
        ret = -1;
    }
    //bypass infra    if ((*((UINT32P) (0x1020d06c)) & 0x0000017f) != 0x0000017f) {
    //bypass infra        dprintf(CRITICAL, "mbist_done 3 is not ready\n");
    //bypass infra        ret = -1;
    //bypass infra    }
    if ((*((UINT32P) (0x14000804)) & 0x007fffff) != 0x007fffff) {
        dprintf(CRITICAL, "mbist_done 4 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x17000024)) & 0x000007ff) != 0x000007ff) {
        dprintf(CRITICAL, "mbist_done 5 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x15000044)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 6 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x15000048)) & 0x00003fff) != 0x00003fff) {
        dprintf(CRITICAL, "mbist_done 7 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x1600003c)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 8 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x16000050)) & 0x0000001f) != 0x0000001f) {
        dprintf(CRITICAL, "mbist_done 9 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x18000068)) & 0x000777ff) != 0x000777ff) {
        dprintf(CRITICAL, "mbist_done a is not ready\n");
        ret = -1;
    }
if(!bypass_md)
{
    //if (!lte_off)
    {
        if ((*((UINT32P) (0x2011002c)) & 0x000fffff) != 0x000fffff) {
            dprintf(CRITICAL, "mbist_done b is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x201d8020)) & 0x00000007) != 0x00000007) {
            dprintf(CRITICAL, "mbist_done c is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x25040024)) & 0x0003ffff) != 0x0003ffff) {
            dprintf(CRITICAL, "mbist_done d is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x2020e020)) & 0x0000000f) != 0x0000000f) {
            dprintf(CRITICAL, "mbist_done e is not ready\n");
            ret = -1;
        }
    }    
    if ((*((UINT32P) (0x23008024)) & 0x000000ff) != 0x000000ff) {
        dprintf(CRITICAL, "mbist_done f is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x27008024)) & 0x000000ff) != 0x000000ff) {
        dprintf(CRITICAL, "mbist_done g is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x22c10024)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done h is not ready\n");
        ret = -1;
    }
    if (!hspa_off)
    {
        if ((*((UINT32P) (0x27230024)) & 0x0000007f) != 0x0000007f) {
            dprintf(CRITICAL, "mbist_done i is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x27440024)) & 0x000001ff) != 0x000001ff) {
            dprintf(CRITICAL, "mbist_done j is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x276e0030)) & 0x000fffff) != 0x000fffff) {
            dprintf(CRITICAL, "mbist_done k is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x278b0028)) & 0xffffffff) != 0xffffffff) {
            dprintf(CRITICAL, "mbist_done l is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x278b002c)) & 0x001fffff) != 0x001fffff) {
            dprintf(CRITICAL, "mbist_done m is not ready\n");
            ret = -1;
        }
    }
    if (!tdd_off)
    {
        if ((*((UINT32P) (0x2405001c)) & 0xffffffff) != 0xffffffff) {
            dprintf(CRITICAL, "mbist_done n is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x24050020)) & 0x0000001f) != 0x0000001f) {
            dprintf(CRITICAL, "mbist_done o is not ready\n");
            ret = -1;
        }
    }
}
    if ((*((UINT32P) (0x13fff200)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done p is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff204)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done q is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff208)) & 0x7fffffff) != 0x7fffffff) {
        dprintf(CRITICAL, "mbist_done r is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff20c)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done s is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff210)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done t is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff214)) & 0x0000000f) != 0x0000000f) {
        dprintf(CRITICAL, "mbist_done u is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff218)) & 0x1fffffff) != 0x1fffffff) {
        dprintf(CRITICAL, "mbist_done v is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff21c)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done w is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff220)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done x is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff224)) & 0x003fffff) != 0x003fffff) {
        dprintf(CRITICAL, "mbist_done y is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff228)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done z is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff22c)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 10 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff230)) & 0x003fffff) != 0x003fffff) {
        dprintf(CRITICAL, "mbist_done 11 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff234)) & 0x00001fff) != 0x00001fff) {
        dprintf(CRITICAL, "mbist_done 12 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff238)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 13 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff23c)) & 0x000003ff) != 0x000003ff) {
        dprintf(CRITICAL, "mbist_done 14 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff240)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 15 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff244)) & 0x00007fff) != 0x00007fff) {
        dprintf(CRITICAL, "mbist_done 16 is not ready\n");
        ret = -1;
    }

    if ((*((UINT32P) (0x1020d070))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 01\n");
    }
    if ((*((UINT32P) (0x1020d074))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 02\n");
    }
    if ((*((UINT32P) (0x1020d078))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 03\n");
    }
    if ((*((UINT32P) (0x14000810))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 04\n");
    }
    if ((*((UINT32P) (0x14000814))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 05\n");
    }
    if ((*((UINT32P) (0x14000818))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 06\n");
    }
    if ((*((UINT32P) (0x17000028))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 07\n");
    }
    if ((*((UINT32P) (0x1700002c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 08\n");
    }
    if ((*((UINT32P) (0x17000030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 09\n");
    }
    if ((*((UINT32P) (0x1500004c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 10\n");
    }
    if ((*((UINT32P) (0x15000050))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 11\n");
    }
    if ((*((UINT32P) (0x15000054))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 12\n");
    }
    if ((*((UINT32P) (0x15000058))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 13\n");
    }
    if ((*((UINT32P) (0x16000040))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 14\n");
    }
    if ((*((UINT32P) (0x16000044))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 15\n");
    }
    if ((*((UINT32P) (0x1800006c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 16\n");
    }
    if ((*((UINT32P) (0x18000070))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 17\n");
    }
    if ((*((UINT32P) (0x18000074))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 18\n");
    }
if(!bypass_md)
{
    if ((*((UINT32P) (0x20110030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 19\n");
    }
    if ((*((UINT32P) (0x20110034))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 20\n");
    }
    if ((*((UINT32P) (0x20110038))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 21\n");
    }
    if ((*((UINT32P) (0x2011003c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 22\n");
    }
    if ((*((UINT32P) (0x201d8024))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 23\n");
    }
    if ((*((UINT32P) (0x25040030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 24\n");
    }
    if ((*((UINT32P) (0x2020e024))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 25\n");
    }
    if ((*((UINT32P) (0x23008030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 26\n");
    }
    if ((*((UINT32P) (0x27008030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 27\n");
    }
    rdata = *((UINT32P) (0x22c10028));
    rdata &= ~(0xfffff801);
    if (rdata != 0x00000000) {
        dprintf(CRITICAL, " sram fail 28\n");
    }
    rdata = *((UINT32P) (0x22c1002c));
    rdata &= ~(0x000fc000);
    if (rdata != 0x00000000) {
        dprintf(CRITICAL, " sram fail 29\n");
    }
    if ((*((UINT32P) (0x27230028))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 30\n");
    }
    if ((*((UINT32P) (0x2723002c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 31\n");
    }
    if ((*((UINT32P) (0x27440028))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 32\n");
    }
    if ((*((UINT32P) (0x2744002c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 33\n");
    }
    if ((*((UINT32P) (0x276e0034))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 34\n");
    }
    if ((*((UINT32P) (0x276e0038))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 35\n");
    }
    if ((*((UINT32P) (0x276e003c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 36\n");
    }
    if ((*((UINT32P) (0x276e0040))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 37\n");
    }
    if ((*((UINT32P) (0x276e0044))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 38\n");
    }
    if ((*((UINT32P) (0x278b0030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 39\n");
    }
    if ((*((UINT32P) (0x278b0034))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 40\n");
    }
    if ((*((UINT32P) (0x24050024))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 41\n");
    }
    if ((*((UINT32P) (0x24050028))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 42\n");
    }
    if ((*((UINT32P) (0x2405002c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 43\n");
    }
    if ((*((UINT32P) (0x24050030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 44\n");
    }
    if ((*((UINT32P) (0x24050034))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 45\n");
    }
}
    if ((*((UINT32P) (0x13fff250))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 46\n");
    }
    if ((*((UINT32P) (0x13fff254))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 47\n");
    }
    if ((*((UINT32P) (0x13fff258))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 48\n");
    }
    if ((*((UINT32P) (0x13fff25c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 49\n");
    }
    if ((*((UINT32P) (0x13fff260))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 50\n");
    }
    if ((*((UINT32P) (0x13fff264))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 51\n");
    }
    if ((*((UINT32P) (0x13fff268))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 52\n");
    }
    if ((*((UINT32P) (0x13fff26c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 53\n");
    }
    if ((*((UINT32P) (0x13fff270))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 54\n");
    }
    if ((*((UINT32P) (0x13fff274))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 55\n");
    }
    if ((*((UINT32P) (0x13fff278))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 56\n");
    }
    if ((*((UINT32P) (0x13fff27c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 57\n");
    }
    if ((*((UINT32P) (0x13fff280))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 58\n");
    }
    if ((*((UINT32P) (0x13fff284))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 59\n");
    }
    if ((*((UINT32P) (0x13fff288))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 60\n");
    }
    if ((*((UINT32P) (0x13fff28c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 61\n");
    }
    if ((*((UINT32P) (0x13fff290))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 62\n");
    }
    if ((*((UINT32P) (0x13fff294))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 63\n");
    }

    dprintf(CRITICAL, "1st round done\n");

    //=== 7) read mbist_fail (before repair);
    //RISCRead  : address 100011c0 data 403fc07f
    //RISCRead  : address 1020d070 data 00000000 Exp 00000fff, ERR at time :            252705453
    //RISCRead  : address 1020d074 data 00000000
    //RISCRead  : address 1020d078 data 00000000 Exp 00040006, ERR at time :            252955443
    //RISCRead  : address 14000810 data 00000000 Exp 83ff8000, ERR at time :            253078515
    //RISCRead  : address 14000814 data 00000000
    //RISCRead  : address 14000818 data 00000000
    //RISCRead  : address 17000028 data 00000000 Exp ffffffff, ERR at time :            253451577
    //RISCRead  : address 1700002c data 00000000 Exp 00ffffff, ERR at time :            253578495
    //RISCRead  : address 17000030 data 00000000
    //RISCRead  : address 1500004c data 00000000 Exp 00003000, ERR at time :            253828485
    //RISCRead  : address 15000050 data 00000000 Exp 8001fb60, ERR at time :            253951557
    //RISCRead  : address 15000054 data 00000000 Exp c00007ed, ERR at time :            254074629
    //RISCRead  : address 15000058 data 00000000 Exp 0000007f, ERR at time :            254197701
    //RISCRead  : address 16000040 data 00000000 Exp 1801e000, ERR at time :            254320773
    //RISCRead  : address 16000044 data 00000000 Exp c0000000, ERR at time :            254443845
    //RISCRead  : address 1800006c data 00000000 Exp 000000ff, ERR at time :            254566917
    //RISCRead  : address 18000070 data 00000000 Exp 00f1f800, ERR at time :            254689989
    //RISCRead  : address 18000074 data 00000000
    //RISCRead  : address 20110030 data 00000000
    //RISCRead  : address 20110034 data 00000000
    //RISCRead  : address 20110038 data 00000000
    //RISCRead  : address 2011003c data 00000000
    //RISCRead  : address 201d8024 data 00000000
    //RISCRead  : address 25040030 data 00000000
    //RISCRead  : address 2020e024 data 00000000
    //RISCRead  : address 23008030 data 00000000
    //RISCRead  : address 27008030 data 00000000
    //RISCRead  : address 22c10028 data fffff801 Exp fffffbef, ERR at time :            256243773
    //RISCRead  : address 22c1002c data 000fc000 Exp 000fd3e0, ERR at time :            256378383
    //RISCRead  : address 27230028 data 00000000
    //RISCRead  : address 2723002c data 00000000
    //RISCRead  : address 27440028 data 00000000 Exp 00000001, ERR at time :            256793751
    //RISCRead  : address 2744002c data 00000000 Exp 000079e0, ERR at time :            256932207
    //RISCRead  : address 276e0034 data 00000000 Exp 00000001, ERR at time :            257070663
    //RISCRead  : address 276e0038 data 00000000 Exp fffff000, ERR at time :            257209119
    //RISCRead  : address 276e003c data 00000000 Exp 000003ff, ERR at time :            257347575
    //RISCRead  : address 276e0040 data 00000000
    //RISCRead  : address 276e0044 data 00000000
    //RISCRead  : address 278b0030 data 00000000 Exp f3c00001, ERR at time :            257762943
    //RISCRead  : address 278b0034 data 00000000 Exp 00000006, ERR at time :            257901399
    //RISCRead  : address 24050024 data 00000000 Exp 00000001, ERR at time :            258051393
    //RISCRead  : address 24050028 data 00000000
    //RISCRead  : address 2405002c data 00000000 Exp 0001fbfc, ERR at time :            258355227
    //RISCRead  : address 24050030 data 00000000 Exp f7f80000, ERR at time :            258509067
    //RISCRead  : address 24050034 data 00000000 Exp 00000001, ERR at time :            258659061
    //RISCRead  : address 13fff250 data 00000000
    //RISCRead  : address 13fff254 data 00000000
    //RISCRead  : address 13fff258 data 00000000
    //RISCRead  : address 13fff25c data 00000000
    //RISCRead  : address 13fff260 data 00000000
    //RISCRead  : address 13fff264 data 00000000
    //RISCRead  : address 13fff268 data 00000000 Exp 000000ff, ERR at time :            259539795
    //RISCRead  : address 13fff26c data 00000000 Exp fffff80X, ERR at time :            259662867
    //RISCRead  : address 13fff270 data 00000000 Exp 000ff7ff, ERR at time :            259785939
    //RISCRead  : address 13fff274 data 00000000
    //RISCRead  : address 13fff278 data 00000000 Exp fffff80X, ERR at time :            260032083
    //RISCRead  : address 13fff27c data 00000000 Exp 000ff7ff, ERR at time :            260155155
    //RISCRead  : address 13fff280 data 00000000
    //RISCRead  : address 13fff284 data 00000000
    //RISCRead  : address 13fff288 data 00000000
    //RISCRead  : address 13fff28c data 00000000
    //RISCRead  : address 13fff290 data 00000000 Exp c0003000, ERR at time :            260770515
    //RISCRead  : address 13fff294 data 00000000
    //=== 8) reg_load_fuse and mbist reset
    *((UINT32P) (0x100011a8)) = 0x000f0000;
if(!bypass_md)
{
    *((UINT32P) (0x20110074)) = 0x00000007;
    *((UINT32P) (0x22c10090)) = 0x000000ff;
    *((UINT32P) (0x27440068)) = 0x00000001;
    *((UINT32P) (0x2744006c)) = 0x00000001;
    *((UINT32P) (0x276e00b0)) = 0x00000001;
    *((UINT32P) (0x278b0060)) = 0x00000001;
    *((UINT32P) (0x278b0064)) = 0x00000001;
    *((UINT32P) (0x278b0068)) = 0x00000001;
    *((UINT32P) (0x26612028)) = 0x00000004;
    *((UINT32P) (0x2660202c)) = 0x00000004;
    *((UINT32P) (0x26632024)) = 0x00000004;
    *((UINT32P) (0x26622040)) = 0x00000004;
    *((UINT32P) (0x26642028)) = 0x00000004;
    *((UINT32P) (0x26652028)) = 0x00000004;
    *((UINT32P) (0x26612028)) = 0x00000000;
    *((UINT32P) (0x2660202c)) = 0x00000000;
    *((UINT32P) (0x26632024)) = 0x00000000;
    *((UINT32P) (0x26622040)) = 0x00000000;
    *((UINT32P) (0x26642028)) = 0x00000000;
    *((UINT32P) (0x26652028)) = 0x00000000;
}
    *((UINT16P) (0x2400001e)) = 0x004d;
    __asm__ __volatile__ ("dsb" : : : "memory");
    *((UINT32P) (0x100011b8)) = 0x00000000;
    *((UINT32P) (0x1020d060)) = 0x00000000;
    *((UINT32P) (0x1020d064)) = 0x00000000;
    *((UINT32P) (0x1400080c)) = 0x00000000;
    *((UINT32P) (0x17000038)) = 0x00000000;
    *((UINT32P) (0x15000064)) = 0x00000000;
    *((UINT32P) (0x15000068)) = 0x00000000;
    *((UINT32P) (0x16000028)) = 0x00000000;
    *((UINT32P) (0x16000048)) = 0x00000000;
    *((UINT32P) (0x1800000c)) = 0x00000000;
if(!bypass_md)
{
    *((UINT32P) (0x20110000)) = 0x00000000;
    *((UINT32P) (0x25040000)) = 0x00000000;
    *((UINT32P) (0x2020e000)) = 0x00000000;
    *((UINT32P) (0x23008000)) = 0x00000000;
    *((UINT32P) (0x27008000)) = 0x00000000;
    *((UINT32P) (0x22c10000)) = 0x00000000;
    *((UINT32P) (0x27230000)) = 0x00000000;
    *((UINT32P) (0x27440000)) = 0x00000000;
    *((UINT32P) (0x276e0000)) = 0x00000000;
    *((UINT32P) (0x278b0000)) = 0x00000000;
    *((UINT32P) (0x24050000)) = 0x00000000;
    *((UINT32P) (0x24050004)) = 0x00000000;
    *((UINT32P) (0x200004b0)) = 0x00000000;
}
    __asm__ __volatile__ ("dsb" : : : "memory");
    //=== 11) reset MBIST (after repair)
    *((UINT32P) (0x100011a8)) = 0x00060000;
    *((UINT32P) (0x1020d050)) = 0x00000000;
    *((UINT32P) (0x14000800)) = 0x00000000;
    *((UINT32P) (0x17000040)) = 0x00000000;
    *((UINT32P) (0x15000060)) = 0x00000000;
    *((UINT32P) (0x18000040)) = 0x00000000;
    *((UINT32P) (0x1600002c)) = 0x00000000;
if(!bypass_md)
{
    *((UINT32P) (0x20110074)) = 0x00000002;
    *((UINT32P) (0x201d8030)) = 0x00000000;
    *((UINT32P) (0x25040040)) = 0x00000000;
    *((UINT32P) (0x2020e028)) = 0x00000000;
    *((UINT32P) (0x23008040)) = 0x00000000;
    *((UINT32P) (0x27008040)) = 0x00000000;
    *((UINT32P) (0x22c10050)) = 0x00000000;
    *((UINT32P) (0x27230044)) = 0x00000000;
    *((UINT32P) (0x27440048)) = 0x00000000;
    *((UINT32P) (0x276e008c)) = 0x00000000;
    *((UINT32P) (0x278b0050)) = 0x00000000;
    *((UINT32P) (0x24050068)) = 0x00000000;
}
    *((UINT32P) (0x13fff310)) = 0x00000000;
if(!bypass_md)
{
    *((UINT32P) (0x2020e028)) = 0x00000000;
}
    __asm__ __volatile__ ("dsb" : : : "memory");
    *((UINT32P) (0x100011a8)) = 0x00070000;
    *((UINT32P) (0x1020d050)) = 0x00010000;
    *((UINT32P) (0x14000800)) = 0x00008000;
    *((UINT32P) (0x17000040)) = 0x00010000;
    *((UINT32P) (0x15000060)) = 0x00000001;
    *((UINT32P) (0x140008a0)) = 0x00000001;
    *((UINT32P) (0x18000040)) = 0x00000001;
    *((UINT32P) (0x1600002c)) = 0x00000001;
if(!bypass_md)
{
    *((UINT32P) (0x20110074)) = 0x00000003;
    *((UINT32P) (0x201d8030)) = 0x00000001;
    *((UINT32P) (0x25040040)) = 0x00000001;
    *((UINT32P) (0x2020e028)) = 0x00000001;
    *((UINT32P) (0x23008040)) = 0x00000001;
    *((UINT32P) (0x27008040)) = 0x00000001;
    *((UINT32P) (0x22c10050)) = 0x00000001;
    *((UINT32P) (0x27230044)) = 0x00000001;
    *((UINT32P) (0x27440048)) = 0x00000001;
    *((UINT32P) (0x276e008c)) = 0x00000001;
    *((UINT32P) (0x278b0050)) = 0x00000001;
    *((UINT32P) (0x24050068)) = 0x00000001;
}
    *((UINT32P) (0x13fff310)) = 0x00000001;
if(!bypass_md)
{
    *((UINT32P) (0x2020e028)) = 0x00000001;
}
    *((UINT32P) (0x13fff00c)) = 0xffffffff;
    *((UINT32P) (0x13fff00c)) = 0x00000000;
    *((UINT32P) (0x13000a10)) = 0x048000b0;
    *((UINT32P) (0x13000a08)) = 0x05511111;
    *((UINT32P) (0x13000000)) = 0x55555555;
    *((UINT32P) (0x13000004)) = 0x55555555;
    *((UINT32P) (0x13000008)) = 0x55555555;
    *((UINT32P) (0x1300000c)) = 0x55555555;
    //=== 12) set mbist_mode = 1; (after repair)
    //bypass infra    *((UINT32P) (0x100011b8)) = 0x00000003;
    *((UINT32P) (0x1020d060)) = 0x03ff0000; //audio only
    //bypass infra    *((UINT32P) (0x1020d064)) = 0xffffffff;
    *((UINT32P) (0x1400080c)) = 0x007fffff;
    *((UINT32P) (0x17000038)) = 0x000007ff;
    *((UINT32P) (0x15000064)) = 0xffffffff;
    *((UINT32P) (0x15000068)) = 0x00003fff;
    *((UINT32P) (0x16000028)) = 0xffffffff;
    *((UINT32P) (0x16000048)) = 0x0000001f;
    *((UINT32P) (0x1800000c)) = 0x0007ffff;
if(!bypass_md)
{
    *((UINT32P) (0x20110000)) = 0x0001ffff;
    *((UINT32P) (0x25040000)) = 0x000000ff;
    *((UINT32P) (0x2020e000)) = 0x00000007;
    *((UINT32P) (0x23008000)) = 0x000000fe;
    *((UINT32P) (0x27008000)) = 0x000000fe;
    *((UINT32P) (0x22c10000)) = 0x7fffffff;
    if (!hspa_off)
    {
        *((UINT32P) (0x27230000)) = 0x0000001f;
        *((UINT32P) (0x27440000)) = 0x0000007f;
        *((UINT32P) (0x276e0000)) = 0x00001fff;
        *((UINT32P) (0x278b0000)) = 0x0000ffff;
    }    
    if (!tdd_off)
    {
        *((UINT32P) (0x24050000)) = 0xffffffff;
        *((UINT32P) (0x24050004)) = 0x0000000f;
    }
}
    *((UINT32P) (0x13fff1a0)) = 0xffffffff;
    *((UINT32P) (0x13fff1a4)) = 0xffffffff;
    *((UINT32P) (0x13fff1a8)) = 0xffffffff;
    *((UINT32P) (0x13fff1ac)) = 0xffffffff;
    *((UINT32P) (0x13fff1b0)) = 0xffffffff;
    *((UINT32P) (0x13fff1b4)) = 0xffffffff;
    *((UINT32P) (0x13fff1b8)) = 0xffffffff;
    *((UINT32P) (0x13fff1bc)) = 0xffffffff;
    *((UINT32P) (0x13fff1c0)) = 0xffffffff;
    *((UINT32P) (0x13fff1c4)) = 0xffffffff;
    *((UINT32P) (0x13fff1c8)) = 0xffffffff;
    *((UINT32P) (0x13fff1cc)) = 0xffffffff;
    *((UINT32P) (0x13fff1d0)) = 0xffffffff;
    *((UINT32P) (0x13fff1d4)) = 0xffffffff;
    *((UINT32P) (0x13fff1d8)) = 0xffffffff;
    *((UINT32P) (0x13fff1dc)) = 0xffffffff;
    *((UINT32P) (0x13fff1e0)) = 0xffffffff;
    *((UINT32P) (0x13fff1e4)) = 0xffffffff;
    *((UINT32P) (0x13fff1e8)) = 0xffffffff;
    *((UINT32P) (0x13fff1ec)) = 0xffffffff;
if(!bypass_md)
{
    //if (!lte_off)
    {
        *((UINT32P) (0x200004b0)) = 0x00000600;
        *((UINT32P) (0x200004b0)) = 0x00000600;
    }
}
    //=== 13) wait mbist_done (after repair)
    //ADD delay here !!!!
    udelay(2000);
    //bypass infra    if ((*((UINT32P) (0x100011c0)) & 0x403fc07f) != 0x403fc07f) {
    //bypass infra        dbgmsg0(CRITICAL, "mbist_done 1 is not ready\n");
    //bypass infra        ret = -1;
    //bypass infra    }
    if ((*((UINT32P) (0x1020d068)) & 0x03ff0000) != 0x03ff0000) {
        dprintf(CRITICAL, "mbist_done 2 is not ready\n");
        ret = -1;
    }
    //bypass infra    if ((*((UINT32P) (0x1020d06c)) & 0x0000017f) != 0x0000017f) {
    //bypass infra        dbgmsg0(CRITICAL, "mbist_done 3 is not ready\n");
    //bypass infra        ret = -1;
    //bypass infra    }
    if ((*((UINT32P) (0x14000804)) & 0x007fffff) != 0x007fffff) {
        dprintf(CRITICAL, "mbist_done 4 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x17000024)) & 0x000007ff) != 0x000007ff) {
        dprintf(CRITICAL, "mbist_done 5 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x15000044)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 6 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x15000048)) & 0x00003fff) != 0x00003fff) {
        dprintf(CRITICAL, "mbist_done 7 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x1600003c)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 8 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x16000050)) & 0x0000001f) != 0x0000001f) {
        dprintf(CRITICAL, "mbist_done 9 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x18000068)) & 0x000777ff) != 0x000777ff) {
        dprintf(CRITICAL, "mbist_done a is not ready\n");
        ret = -1;
    }
if(!bypass_md)
{
    //if (!lte_off)
    { 
        if ((*((UINT32P) (0x2011002c)) & 0x000fffff) != 0x000fffff) {
            dprintf(CRITICAL, "mbist_done b is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x201d8020)) & 0x00000007) != 0x00000007) {
            dprintf(CRITICAL, "mbist_done c is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x25040024)) & 0x0003ffff) != 0x0003ffff) {
            dprintf(CRITICAL, "mbist_done d is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x2020e020)) & 0x0000000f) != 0x0000000f) {
            dprintf(CRITICAL, "mbist_done e is not ready\n");
            ret = -1;
        }
    }    
    if ((*((UINT32P) (0x23008024)) & 0x000000ff) != 0x000000ff) {
        dprintf(CRITICAL, "mbist_done f is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x27008024)) & 0x000000ff) != 0x000000ff) {
        dprintf(CRITICAL, "mbist_done g is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x22c10024)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done h is not ready\n");
        ret = -1;
    }
    if (!hspa_off)
    {
        if ((*((UINT32P) (0x27230024)) & 0x0000007f) != 0x0000007f) {
            dprintf(CRITICAL, "mbist_done i is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x27440024)) & 0x000001ff) != 0x000001ff) {
            dprintf(CRITICAL, "mbist_done j is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x276e0030)) & 0x000fffff) != 0x000fffff) {
            dprintf(CRITICAL, "mbist_done k is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x278b0028)) & 0xffffffff) != 0xffffffff) {
            dprintf(CRITICAL, "mbist_done l is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x278b002c)) & 0x001fffff) != 0x001fffff) {
            dprintf(CRITICAL, "mbist_done m is not ready\n");
            ret = -1;
        }
    }    
    if (!tdd_off)
    {
        if ((*((UINT32P) (0x2405001c)) & 0xffffffff) != 0xffffffff) {
            dprintf(CRITICAL, "mbist_done n is not ready\n");
            ret = -1;
        }
        if ((*((UINT32P) (0x24050020)) & 0x0000001f) != 0x0000001f) {
            dprintf(CRITICAL, "mbist_done o is not ready\n");
            ret = -1;
        }
    }
}
    if ((*((UINT32P) (0x13fff200)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done p is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff204)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done q is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff208)) & 0x7fffffff) != 0x7fffffff) {
        dprintf(CRITICAL, "mbist_done r is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff20c)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done s is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff210)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done t is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff214)) & 0x0000000f) != 0x0000000f) {
        dprintf(CRITICAL, "mbist_done u is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff218)) & 0x1fffffff) != 0x1fffffff) {
        dprintf(CRITICAL, "mbist_done v is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff21c)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done w is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff220)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done x is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff224)) & 0x003fffff) != 0x003fffff) {
        dprintf(CRITICAL, "mbist_done y is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff228)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done z is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff22c)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 10 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff230)) & 0x003fffff) != 0x003fffff) {
        dprintf(CRITICAL, "mbist_done 11 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff234)) & 0x00001fff) != 0x00001fff) {
        dprintf(CRITICAL, "mbist_done 12 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff238)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 13 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff23c)) & 0x000003ff) != 0x000003ff) {
        dprintf(CRITICAL, "mbist_done 14 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff240)) & 0xffffffff) != 0xffffffff) {
        dprintf(CRITICAL, "mbist_done 15 is not ready\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff244)) & 0x00007fff) != 0x00007fff) {
        dprintf(CRITICAL, "mbist_done 16 is not ready\n");
        ret = -1;
    }
    //=== 15) read mbist_fail (after repair);
    //bypass infra    if ((*((UINT32P) (0x100011c0)) & 0x0000c000) != 0x00000000) {
    //bypass infra        dprintf(CRITICAL, " sram fail 01\n");
    //bypass infra        ret = -1;
    //bypass infra    }
    if ((*((UINT32P) (0x1020d070))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 01\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x1020d074))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 02\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x1020d078))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 03\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x14000810))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 04\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x14000814))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 05\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x14000818))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 06\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x17000028))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 07\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x1700002c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 08\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x17000030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 09\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x1500004c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 10\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x15000050))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 11\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x15000054))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 12\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x15000058))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 13\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x16000040))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 14\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x16000044))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 15\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x1800006c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 16\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x18000070))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 17\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x18000074))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 18\n");
        ret = -1;
    }
if(!bypass_md)
{
    if ((*((UINT32P) (0x20110030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 19\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x20110034))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 20\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x20110038))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 21\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x2011003c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 22\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x201d8024))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 23\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x25040030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 24\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x2020e024))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 25\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x23008030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 26\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x27008030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 27\n");
        ret = -1;
    }
    rdata = *((UINT32P) (0x22c10028));
    rdata &= ~(0xfffff801);
    if (rdata != 0x00000000) {
        dprintf(CRITICAL, " sram fail 28\n");
        ret = -1;
    }
    rdata = *((UINT32P) (0x22c1002c));
    rdata &= ~(0x000fc000);
    if (rdata != 0x00000000) {
        dprintf(CRITICAL, " sram fail 29\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x27230028))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 30\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x2723002c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 31\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x27440028))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 32\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x2744002c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 33\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x276e0034))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 34\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x276e0038))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 35\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x276e003c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 36\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x276e0040))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 37\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x276e0044))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 38\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x278b0030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 39\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x278b0034))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 40\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x24050024))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 41\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x24050028))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 42\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x2405002c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 43\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x24050030))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 44\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x24050034))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 45\n");
        ret = -1;
    }
}
    if ((*((UINT32P) (0x13fff250))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 46\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff254))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 47\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff258))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 48\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff25c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 49\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff260))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 50\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff264))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 51\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff268))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 52\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff26c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 53\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff270))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 54\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff274))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 55\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff278))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 56\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff27c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 57\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff280))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 58\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff284))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 59\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff288))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 60\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff28c))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 61\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff290))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 62\n");
        ret = -1;
    }
    if ((*((UINT32P) (0x13fff294))) != 0x00000000) {
        dprintf(CRITICAL, " sram fail 63\n");
        ret = -1;
    }

    dprintf(CRITICAL, "2nd round done\n");

    // mbist reset
    dprintf(CRITICAL, "Reset mbist\n");
    *((UINT32P) (0x100011b8)) = 0x00000000;
    *((UINT32P) (0x1020d060)) = 0x00000000;
    *((UINT32P) (0x1020d064)) = 0x00000000;
    *((UINT32P) (0x1400080c)) = 0x00000000;
    *((UINT32P) (0x17000038)) = 0x00000000;
    *((UINT32P) (0x15000064)) = 0x00000000;
    *((UINT32P) (0x15000068)) = 0x00000000;
    *((UINT32P) (0x16000028)) = 0x00000000;
    *((UINT32P) (0x16000048)) = 0x00000000;
    *((UINT32P) (0x1800000c)) = 0x00000000;
if(!bypass_md)
{
    *((UINT32P) (0x20110000)) = 0x00000000;
    *((UINT32P) (0x25040000)) = 0x00000000;
    *((UINT32P) (0x2020e000)) = 0x00000000;
    *((UINT32P) (0x23008000)) = 0x00000000;
    *((UINT32P) (0x27008000)) = 0x00000000;
    *((UINT32P) (0x22c10000)) = 0x00000000;
    *((UINT32P) (0x27230000)) = 0x00000000;
    *((UINT32P) (0x27440000)) = 0x00000000;
    *((UINT32P) (0x276e0000)) = 0x00000000;
    *((UINT32P) (0x278b0000)) = 0x00000000;
    *((UINT32P) (0x24050000)) = 0x00000000;
    *((UINT32P) (0x24050004)) = 0x00000000;
    *((UINT32P) (0x200004b0)) = 0x00000000;
}
    __asm__ __volatile__ ("dsb" : : : "memory");
#endif

    /* Reset MIPI */
    dprintf(CRITICAL, "Reset MIPI\n");
    DSI_PHY_clk_switch(DISP_MODULE_DSI0, NULL, 0);
    DSI_PHY_clk_switch(DISP_MODULE_DSI1, NULL, 0);
    
    /* Disable MMSYS clock */
    dprintf(CRITICAL, "Disable MMSYS clock\n");
    *((UINT32P) (0x14000100)) = 0xffffffff;
    *((UINT32P) (0x14000110)) = 0xffffffff;

    dprintf(CRITICAL, "Restore USB clock setting\n");
    *((UINT32P) (0x11280700)) = 0x31001;
    dprintf(CRITICAL, "0x11280700 = 0x%08X\n", *((UINT32P) (0x11280700)));

    dprintf(CRITICAL, "Restore DCM setting on AP_DMA\n");
    *((UINT32P) (0x11000070)) = rstd_reg;
    dprintf(CRITICAL, "0x11000070 = 0x%08X\n", *((UINT32P) (0x11000070)));
    
    /* Reset MD*/
    dprintf(CRITICAL, "Reset MD\n");
    rgu_swsys_reset(WD_MD_RST);

    dprintf(CRITICAL, "repair_sram END\n");

    return ret;
}
