#ifndef _MT_PMIC_LK_SW_H_
#define _MT_PMIC_LK_SW_H_

#include <platform/mt_typedefs.h>

//==============================================================================
// PMIC Define
//==============================================================================

#define PMIC6331_E1_CID_CODE    0x3110
#define PMIC6331_E2_CID_CODE    0x3120
#define PMIC6331_E3_CID_CODE    0x3130

#define PMIC6332_E1_CID_CODE    0x3210
#define PMIC6332_E2_CID_CODE    0x3220
#define PMIC6332_E3_CID_CODE    0x3230


#if 0
#define VBAT_CHANNEL_NUMBER      7
#define ISENSE_CHANNEL_NUMBER	 6
#define VCHARGER_CHANNEL_NUMBER  4
#define VBATTEMP_CHANNEL_NUMBER  5
#else

#define AUXADC_CHANNEL_MASK	0x0f
#define AUXADC_CHANNEL_SHIFT	0
#define AUXADC_CHIP_MASK	0x0f
#define AUXADC_CHIP_SHIFT	4
#define AUXADC_USER_MASK	0x0f
#define AUXADC_USER_SHIFT	8
#define CLEAR_REQ		0
#define SET_REQ			1	
#define ONLY_REQ		2

/* ADC Channel Number */
typedef enum {
	//MT6331
	AUX_TSENSE_31_AP = 	0x004,
	AUX_VACCDET_AP,
	AUX_VISMPS_1_AP,
	AUX_ADCVIN0_AP,
	AUX_HP_AP = 		0x009,
        
	//MT6332
	AUX_BATSNS_AP = 	0x010,
	AUX_ISENSE_AP,
	AUX_VBIF_AP,
	AUX_BATON_AP,
	AUX_TSENSE_32_AP,
	AUX_VCHRIN_AP,
	AUX_VISMPS_2_AP,
	AUX_VUSB_AP,
	AUX_M3_REF_AP,   
	AUX_SPK_ISENSE_AP,
	AUX_SPK_THR_V_AP,
	AUX_SPK_THR_I_AP,

	AUX_VADAPTOR_AP =	0x027,        
	AUX_TSENSE_31_MD = 	0x104,
	AUX_ADCVIN0_MD = 	0x107,
	AUX_TSENSE_32_MD =	0x114,
	AUX_ADCVIN0_GPS = 	0x208
} upmu_adc_chl_list_enum;

typedef enum {
	AP = 0,
	MD,
	GPS,
	AUX_USER_MAX	
} upmu_adc_user_list_enum;

typedef enum {
	MT6331_CHIP = 0,
	MT6332_CHIP,
	ADC_CHIP_MAX
} upmu_adc_chip_list_enum;

#endif

typedef enum {
    CHARGER_UNKNOWN = 0,
    STANDARD_HOST,          // USB : 450mA
    CHARGING_HOST,
    NONSTANDARD_CHARGER,    // AC : 450mA~1A 
    STANDARD_CHARGER,       // AC : ~1A
    APPLE_2_1A_CHARGER,     // 2.1A apple charger
    APPLE_1_0A_CHARGER,     // 1A apple charger
    APPLE_0_5A_CHARGER,     // 0.5A apple charger
} CHARGER_TYPE;


//==============================================================================
// PMIC Exported Function
//==============================================================================
extern U32 pmic_read_interface (U32 RegNum, U32 *val, U32 MASK, U32 SHIFT);
extern U32 pmic_config_interface (U32 RegNum, U32 val, U32 MASK, U32 SHIFT);
extern U32 pmic_IsUsbCableIn (void);
extern kal_bool upmu_is_chr_det(void);
extern int pmic_detect_powerkey(void);
extern int pmic_detect_powerkey(void);
extern kal_uint32 upmu_get_reg_value(kal_uint32 reg);
extern void PMIC_DUMP_ALL_Register(void);
extern U32 pmic_init (void);
extern int PMIC_IMM_GetOneChannelValue(upmu_adc_chl_list_enum dwChannel, int deCount, int trimd);
extern int get_bat_sense_volt(int times);
extern int get_i_sense_volt(int times);
extern int get_charger_volt(int times);
extern int get_tbat_volt(int times);
extern CHARGER_TYPE mt_charger_type_detection(void);

//==============================================================================
// PMIC Status Code
//==============================================================================
#define PMIC_TEST_PASS               0x0000
#define PMIC_TEST_FAIL               0xB001
#define PMIC_EXCEED_I2C_FIFO_LENGTH  0xB002
#define PMIC_CHRDET_EXIST            0xB003
#define PMIC_CHRDET_NOT_EXIST        0xB004

//==============================================================================
// PMIC Register Index
//==============================================================================
#include "upmu_hw.h"

#endif // _MT_PMIC_LK_SW_H_

