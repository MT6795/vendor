#include <utils/Log.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include "camera_custom_nvram.h"
#include "camera_custom_lens.h"
const NVRAM_LENS_PARA_STRUCT DW9761BAF_LENS_PARA_DEFAULT_VALUE = 
{1, // Version

rFocusRange: {0, // i4InfPos
1023, // i4MacroPos
},

rAFNVRAM: {
sAF_Coef: {
sTABLE: {282, // i4Offset
17, // i4NormalNum
17, // i4MacroNum
0, // i4InfIdxOffset
1, // i4MacroIdxOffset
{0, 12, 27, 42, 62, 82, 102, 125, 148, 171, 194, 218, 242, 266, 290, 314, 338, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4Pos
},
15, // i4THRES_MAIN
10, // i4THRES_SUB
1, // i4AFC_FAIL_CNT
0, // i4FAIL_POS
4, // i4INIT_WAIT
{500, 500, 500, 500, 500, },// i4FRAME_WAIT
0, // i4DONE_WAIT
},

sVAFC_Coef: {
sTABLE: {282, // i4Offset
17, // i4NormalNum
17, // i4MacroNum
0, // i4InfIdxOffset
1, // i4MacroIdxOffset
{0, 12, 27, 42, 62, 82, 102, 125, 148, 171, 194, 218, 242, 266, 290, 314, 338, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4Pos
},
15, // i4THRES_MAIN
10, // i4THRES_SUB
1, // i4AFC_FAIL_CNT
-1, // i4FAIL_POS
4, // i4INIT_WAIT
{500, 500, 500, 500, 500, },// i4FRAME_WAIT
0, // i4DONE_WAIT
},

sAF_TH: {8, // i4ISONum
{100, 150, 200, 300, 400, 600, 800, 1600, },// i4ISO
{8, 19, 21, 20, 20, 20, 19, 8, 15, 29, 30, 30, 29, 29, 29, 15, 26, 42, 43, 43, 
43, 42, 42, 26, 45, 61, 62, 62, 62, 61, 61, 44, 72, 88, 89, 89, 89, 88, 88, 72, 
113, 126, 127, 127, 127, 126, 126, 113, 172, 180, 180, 180, 180, 180, 180, 172, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, },// i4MIN_TH
{3, 3, 3, 4, 4, 5, 5, 8, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{6, 5, 6, 6, 7, 8, 8, 12, },// i4HW_TH2
},

sZSD_AF_TH: {8, // i4ISONum
{100, 150, 200, 300, 400, 600, 800, 1600, },// i4ISO
{20, 20, 19, 19, 18, 18, 17, 9, 29, 29, 29, 28, 28, 27, 26, 17, 43, 42, 42, 42, 
41, 40, 40, 29, 62, 61, 61, 61, 60, 60, 59, 48, 89, 88, 88, 88, 87, 87, 86, 76, 
126, 126, 126, 126, 125, 125, 124, 116, 180, 180, 180, 180, 179, 179, 179, 174, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, },// i4MIN_TH
{4, 4, 5, 6, 7, 8, 9, 12, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{8, 8, 9, 10, 11, 14, 16, 21, },// i4HW_TH2
},

sVID_AF_TH: {8, // i4ISONum
{100, 150, 200, 300, 400, 600, 800, 1600, },// i4ISO
{20, 20, 19, 19, 18, 18, 17, 9, 29, 29, 29, 28, 28, 27, 26, 17, 43, 42, 42, 42, 
41, 40, 40, 29, 62, 61, 61, 61, 60, 60, 59, 48, 89, 88, 88, 88, 87, 87, 86, 76, 
126, 126, 126, 126, 125, 125, 124, 116, 180, 180, 180, 180, 179, 179, 179, 174, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, },// i4MIN_TH
{4, 4, 5, 6, 7, 8, 9, 12, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{8, 8, 9, 10, 11, 14, 16, 21, },// i4HW_TH2
},

sVID1_AF_TH: {0, // i4ISONum
{0, 0, 0, 0, 0, 0, 0, 0, },// i4ISO
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH2
},

sVID2_AF_TH: {0, // i4ISONum
{0, 0, 0, 0, 0, 0, 0, 0, },// i4ISO
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH2
},

sIHDR_AF_TH: {0, // i4ISONum
{0, 0, 0, 0, 0, 0, 0, 0, },// i4ISO
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH2
},

sREV1_AF_TH: {0, // i4ISONum
{0, 0, 0, 0, 0, 0, 0, 0, },// i4ISO
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH2
},

sREV2_AF_TH: {0, // i4ISONum
{0, 0, 0, 0, 0, 0, 0, 0, },// i4ISO
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH2
},

sREV3_AF_TH: {0, // i4ISONum
{0, 0, 0, 0, 0, 0, 0, 0, },// i4ISO
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH2
},

sREV4_AF_TH: {0, // i4ISONum
{0, 0, 0, 0, 0, 0, 0, 0, },// i4ISO
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH2
},

sREV5_AF_TH: {0, // i4ISONum
{0, 0, 0, 0, 0, 0, 0, 0, },// i4ISO
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4GMR
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH
{0, 0, 0, 0, 0, 0, 0, 0, },// i4FV_DC2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4MIN_TH2
{0, 0, 0, 0, 0, 0, 0, 0, },// i4HW_TH2
},
1, // i4ReadOTP
3, // i4StatGain
0, // i4LV_THRES
170, // i4InfPos
33, // i4FRAME_TIME
{0, 100, 200, 350, 500, },// i4BackJump
500, // i4BackJumpPos
20, // i4AFC_STEP_SIZE
18, // i4SPOT_PERCENT_W
24, // i4SPOT_PERCENT_H
0, // i4CHANGE_CNT_DELTA
1, // i4AFS_STEP_MIN_ENABLE
4, // i4AFS_STEP_MIN_NORMAL
4, // i4AFS_STEP_MIN_MACRO
5, // i4FIRST_FV_WAIT
12, // i4FV_1ST_STABLE_THRES
10000, // i4FV_1ST_STABLE_OFFSET
6, // i4FV_1ST_STABLE_NUM
6, // i4FV_1ST_STABLE_CNT
50, // i4FV_SHOCK_THRES
30000, // i4FV_SHOCK_OFFSET
5, // i4FV_VALID_CNT
20, // i4FV_SHOCK_FRM_CNT
5, // i4FV_SHOCK_CNT
80, // i4FDWinPercent
40, // i4FDSizeDiff
15, // i4FD_DETECT_CNT
5, // i4FD_NONE_CNT
0, // i4LeftSearchEnable
0, // i4LeftJumpStep
0, // i4Curve5ptFit
0, // i4AfDoneDelay
0, // i4VdoAfDoneDelay
0, // i4ZoomInWinChg
1, // i4SensorEnable
77, // i4PostureComp
2, // i4SceneMonitorLevel
1, // i4VdoSceneMonitorLevel

sFV: {1, // i4Enable
0, // i4ChgType
100000, // i4ChgOffset
{40, 40, 30, },// i4ChgThr
{15, 10, 10, },// i4ChgCnt
1, // i4StbType
50000, // i4StbOffset
{25, 25, 25, },// i4StbThr
{10, 7, 5, },// i4StbCnt
},

sGS: {1, // i4Enable
0, // i4ChgType
10, // i4ChgOffset
{20, 20, 15, },// i4ChgThr
{15, 10, 10, },// i4ChgCnt
1, // i4StbType
5, // i4StbOffset
{5, 10, 10, },// i4StbThr
{10, 7, 5, },// i4StbCnt
},

sAEB: {1, // i4Enable
0, // i4ChgType
15, // i4ChgOffset
{30, 30, 25, },// i4ChgThr
{33, 30, 30, },// i4ChgCnt
1, // i4StbType
5, // i4StbOffset
{10, 10, 10, },// i4StbThr
{10, 7, 5, },// i4StbCnt
},

sGYRO: {1, // i4Enable
0, // i4ChgType
0, // i4ChgOffset
{40, 40, 20, },// i4ChgThr
{5, 3, 3, },// i4ChgCnt
1, // i4StbType
0, // i4StbOffset
{10, 20, 10, },// i4StbThr
{10, 5, 5, },// i4StbCnt
},

sACCE: {1, // i4Enable
0, // i4ChgType
0, // i4ChgOffset
{80, 80, 60, },// i4ChgThr
{15, 12, 12, },// i4ChgCnt
1, // i4StbType
0, // i4StbOffset
{50, 50, 50, },// i4StbThr
{10, 7, 5, },// i4StbCnt
},

sVdoFV: {1, // i4Enable
0, // i4ChgType
100000, // i4ChgOffset
{40, 40, 30, },// i4ChgThr
{20, 15, 15, },// i4ChgCnt
1, // i4StbType
50000, // i4StbOffset
{20, 20, 20, },// i4StbThr
{20, 15, 10, },// i4StbCnt
},

sVdoGS: {1, // i4Enable
0, // i4ChgType
15, // i4ChgOffset
{20, 20, 15, },// i4ChgThr
{28, 25, 25, },// i4ChgCnt
1, // i4StbType
5, // i4StbOffset
{5, 5, 5, },// i4StbThr
{15, 13, 10, },// i4StbCnt
},

sVdoAEB: {1, // i4Enable
0, // i4ChgType
15, // i4ChgOffset
{30, 30, 25, },// i4ChgThr
{33, 30, 30, },// i4ChgCnt
1, // i4StbType
5, // i4StbOffset
{10, 10, 10, },// i4StbThr
{15, 13, 10, },// i4StbCnt
},

sVdoGYRO: {1, // i4Enable
0, // i4ChgType
0, // i4ChgOffset
{40, 40, 20, },// i4ChgThr
{7, 5, 5, },// i4ChgCnt
1, // i4StbType
0, // i4StbOffset
{10, 10, 10, },// i4StbThr
{15, 13, 10, },// i4StbCnt
},

sVdoACCE: {1, // i4Enable
0, // i4ChgType
0, // i4ChgOffset
{80, 80, 60, },// i4ChgThr
{15, 12, 12, },// i4ChgCnt
1, // i4StbType
0, // i4StbOffset
{50, 50, 50, },// i4StbThr
{13, 13, 10, },// i4StbCnt
},
1, // i4FvExtractEnable
30, // i4FvExtractThr
1, // i4DampingCompEnable
{15, 31, 47, 63, 79, 94, 110, 126, 142, 158, 0, 0, 0, 0, 0, },// i4DampingStep
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4DampingRdirComp
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },// i4DampingLdirComp
0, // i4DirSelectEnable
-1, // i4InfDir
-1, // i4MidDir
-1, // i4MacDir
40, // i4RatioInf
70, // i4RatioMac
1, // i4StartBoundEnable
3, // i4StartCamCAF
3, // i4StartCamTAF
0, // i4StartVdoCAF
0, // i4StartVdoTAF
1, // i4FpsRemapTblEnable
{10, 12, },// i4FpsThres
{0, 0, },// i4TableClipPt
{80, 90, },// i4TableRemapPt
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
},// i4EasyTuning
{12, 100, 10, 30, 70, 100, 1, 100, 100, 100, 5, 3, 0, 8, -3, -1, 2, -15, -4, 0, 
4, -2, 0, 11, -1, -1, 2, 15, 1, 9, 2, 100, 100, 100, 2, -1, -2, -1, 16, 5, 
-9, 0, -1, -1, 1, -2, 5, 1, -7, 5, -9, 0, 0, 2, 3, 100, 100, 100, -4, 1, 
-17, -7, 21, -12, -4, 0, -7, -2, 5, 7, 8, 3, 35, -12, -4, 0, 0, 22, 4, 100, 
100, 100, -4, -14, -4, -22, -11, -21, 4, -7, -45, -45, 6, -5, 15, 1, 34, -21, 4, 7, 
0, 82, 5, 100, 100, 100, -3, -11, -3, -17, -8, -16, 3, -5, -36, -36, 4, -4, 12, 0, 
27, -16, 3, 5, 0, 65, 6, 100, 100, 100, -2, -9, -2, -14, -6, -13, 2, -4, -30, -30, 
3, -3, 10, 0, 22, -13, 2, 4, 0, 54, 7, 100, 100, 100, -1, -7, -1, -12, -5, -11, 
1, -3, -25, -25, 2, -2, 8, 0, 18, -11, 1, 3, 0, 46, 8, 100, 100, 100, 0, -6, 
0, -10, -4, -9, 0, -2, -21, -21, 1, -1, 7, 0, 15, -9, 0, 2, 0, 40, 9, 100, 
100, 100, 0, -5, 0, -8, -3, -8, 0, -1, -18, -18, 0, 0, 6, 0, 13, -8, 0, 1, 
0, 35, 10, 100, 100, 100, 0, -4, 0, -7, -2, -7, 0, 0, -16, -16, 0, 0, 5, 0, 
11, -7, 0, 0, 0, 31, 11, 100, 100, 100, 0, -3, 0, -6, -1, -6, 0, 0, -14, -14, 
0, 0, 4, 0, 10, -6, 0, 0, 0, 28, 12, 100, 100, 100, 0, -2, 0, -5, 0, -5, 
0, 0, -12, -12, 0, 0, 3, 0, 9, -5, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
},// i4DepthAF
{0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 10, 40, 5, 100, 50, 150, 50, 0, 
0, 1, 50110050, 110120070, 5, 100, 50, 150, 1050050, 70, 70, 25, 25, 2, 1014, 55030, 3003, 7, 1001, 50, 
14, 1, 800, 170000, 250, 1500000, 99, 16, 15, 0, 82, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
},// i4Coefs
},

rPDNVRAM: {
rCaliData: {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, },// uData
0, // i4Size
},

rTuningData: {32, // i4FocusPDSizeX
24, // i4FocusPDSizeY
{30, 150, 200, 300, 400, },// i4ConfIdx1
{384, 410, 435, 461, 486, },// i4ConfIdx2
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 20, 20, 20, 20, 0, 20, 
60, 60, 60, 60, 0, 20, 60, 60, 60, 60, 0, 20, 60, 60, 60, 100, },// i4ConfTbl
230, // i4SaturateLevel
1, // i4SaturateThr
10, // i4ConfThr
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
},// i4Reserved
},
},
};
